class ClusterSQL():
	"""Import cluster data into a SQLite database"""
	import sqlite3	
	
	def __init__(self):
		conn = sqlite3.connect('cluster.db')
		c = self.conn.cursor()
	
	def create_cluster(self):
		"""This is only run once to create the table"""		
		self.c.execute('''CREATE TABLE cluster (name text, redshift real, AngScale real, Dlum real, nH20 real, kT real)''')	
		self.metadata = {'name':0} # link cluster parameters to header names
	def cluster_import(self,data):
		"""import cluster data into database"""

		self.c.execute("INSERT INTO cluster VALUES ('2006-01-05','BUY','RHAT',100,35.14)")
		self.c.executemany('INSERT INTO cluster VALUES (?,?,?,?,?)', data)		

		# Larger example that inserts many records at a time
		#data = [('2006-03-28', 'BUY', 'IBM', 1000, 45.00),
             #('2006-04-05', 'BUY', 'MSFT', 1000, 72.00),
             #('2006-04-06', 'SELL', 'IBM', 500, 53.00),
            #]
		
		conn.commit()
		conn.close()

	def get_cluster_data(self,ClusterName):
		
		"""get cluster data from database""" 
		c.execute('SELECT * FROM cluster WHERE symbol=?', ClusterName)
		print c.fetchone()
		conn.close()
	
	def __del__(self):
		conn.close()





###############
# using sqlalchemy instead

from sqlalchemy import *

db = create_engine('sqlite:///tutorial.db')

db.echo = False  # Try changing this to True and see what happens

metadata = BoundMetaData(db)

users = Table('users', metadata,
    Column('user_id', Integer, primary_key=True),
    Column('name', String(40)),
    Column('age', Integer),
    Column('password', String),
)
users.create()

i = users.insert()
i.execute(name='Mary', age=30, password='secret')
i.execute({'name': 'John', 'age': 42},
          {'name': 'Susan', 'age': 57},
          {'name': 'Carl', 'age': 33})

s = users.select()
rs = s.execute()

row = rs.fetchone()
print 'Id:', row[0]
print 'Name:', row['name']
print 'Age:', row.age
print 'Password:', row[users.c.password]

for row in rs:
    print row.name, 'is', row.age, 'years old'

