def find_files():
	#### I think this is a duplicate. Should have a general find_files() now
	
	"""Will find all of the standard reprocessed files needed for the modules"""
	import glob
	import os
	file_dict ={}
	file_dict['directory'] = os.getcwd()
	file_dict['mskfile'] = glob.glob('acis*msk1.fits')[0]
	file_dict['badpixfile'] = glob.glob('acis*bpix1.fits')[0]
	file_dict['origevents'] = glob.glob('acis*evt2.fits')[0]
	file_dict['pbkfile'] = glob.glob('acis*pbk0.fits')[0]
	file_dict['asolfile'] = glob.glob('pcadf*asol1.fits')[0]
	return file_dict
	
def ann_spec(binspec=25):
	"""MODULE: Determine Annuli for spectral fit and fit spectra
	
	Keyword Arguments
		binspec -- Number of counts to bin the spectra
	
	"""
	### make sure lists of spectral extration regions are defined
	
	import os
	import ciao_contrib.runtool as rt
	import determine_spectral_annulus as dsa
	import glob
	
	# search directory to see if *.lis exists
	multi = glob.glob('multi*.lis')
	
	if len(multi) not 3:
		print("Please determine the annuli and create the annular lists before continuing")
		regions = dsa.determine_spectral_annulus()
	
	FILE_LIST = find_files()
	
	rt.specextract.punlearn()
	rt.specextract.infile = "multi_src.lis"
	rt.specextract.outroot = "multi_out.lis"
	rt.specextract.bkgfile = "multi_bkg.lis"
	rt.specextract.binspec = binspec
	rt.specextract.pbkfile = FILE_LIST['pbkfile']
	rt.specextract.mskfile = FILE_LIST['mskfile']
	rt.specextract.badpixfile = FILE_LIST['badpixfile']
	rt.specextract.asp = FILE_LIST['asolfile']
	rt.specextract()
	
	
	