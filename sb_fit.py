def singlebeta(radius=None, sbfit=None, guess=None):
	"""
	Fit cluster surface brightness with single beta model
	
	Keyword Arguments:
		radius -- Numpy array which contains the radii in kpc
		sb -- Numpy array of the same length as radius which contains sb 
	"""
	
	from scipy.optimize import curve_fit
	
	def entr_func(radius,ampl,r0,beta,xpos):
		return ampl*(1+(radius-xpos/r0)**2)**(-3*beta+0.5)
		
	fit_param,fit_err = curve_fit(entr_func,radius,sb,p0=guess,sigma=entropy_err)
		
	return fit_param,fit_err
	
def main():	
	import csv
	import numpy as np
	import matplotlib.pyplot as plt
	import matplotlib.gridspec as gridspec
	from matplotlib import rc
	rc('font',**{'family':'sans-serif','sans-serif':['Helvetica']})
	## for Palatino and other serif fonts use:
	#rc('font',**{'family':'serif','serif':['Palatino']})
	rc('text', usetex=True)
	
	filename = 'E:/Scripts/SethScripts/outputs/'
	for line in file:
		