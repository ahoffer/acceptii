def xspec_extract(MyCluster=None,cluster_params=None):
	"""Open up Xspec log files and get the relevant fits and errors.
	
	Keyword Arguments:
	MyCluster -- Cluster class with cluster parameters and Xspec parameters
	cluster_params -- Temporary measure before getting MyCluster class working
	
	"""
	
	import re
	import os
	import logging
	
	if MyCluster:
		# Still need to get this class working
		
		# Check to make sure the values are there
		cluster_params = MyCluster.get_cluster_params()
		xspec_params = MyCluster.get_xspec_params()
		
		name = cluster_params['Name'] 
		obsid = cluster_params['Obsid'] 
		z = cluster_params['Redshift'] 
		nh = cluster_params['nH'] # 10^20 cm^-2
		
		xspec_file = xspec_params['Xspecfile'] # Use full path
		reg_num = xspec_params['num_regions'] 
		reg_prefix = xspec_params['reg_prefix']  
		reg_suffix = xspec_params['reg_suffix'] 
	#else:
		## Uncomment once MyCluster solution works
		#MyCluster.set_cluster_params()
		#MyCluster.set_xspec_params()
		
		
		
	##### Temporary solution until I get it working as a class
	cluster_params={'Name': 'MS2137','Redshift': 0.313,'nH': 3.4,'Obsid': 10003,'Xspecfile': '/home/Aaron/Xray/inputs/MS2137/xspec_tx_fit_ms2137_unbin2013-02-26T19:15:37.log','num_regions': 7,'reg_prefix': '/home/Aaron/Xray/inputs/MS2137/annulus','reg_suffix': '.reg'}

	if cluster_params:
		name = cluster_params['Name'] 
		obsid = cluster_params['Obsid'] 
		z = cluster_params['Redshift'] 
		nh = cluster_params['nH'] # 10^20 cm^-2
		xspec_file = cluster_params['Xspecfile'] # Use full path
		reg_num = cluster_params['num_regions'] 
		reg_prefix = cluster_params['reg_prefix']  
		reg_suffix = cluster_params['reg_suffix'] 
		
	TX2 = 0.6 # Temperature of soft X-ray foreground fixed based on Ken's thesis.
	norm1 = [] # Only positive normalization for single model. 
	norm2 = [] # Normalization computed and allowed to be negative.
	cr = [] # Count rate in cts/second based on counts in the 0.5-2 keV range given in the SB profile (XSPEC VALUES?)
	rin = [] # Inner radius for annuli.
	rout = [] # Outer radius for annuli.
	SRC = 1 # Not sure what this does in the de_ files... 
	PARAMLEN = 13 # Number of parameters used in the Xspec models. 
	
	# def read_annuli():
	#	"""Import region file to get the inner and outer radii."""
	i = 0
	while(i < reg_num):
		reg_file = reg_prefix+str(i)+reg_suffix
		reg = open(reg_file).readline()
		reg = reg.strip('anlus()\n')
		reg = reg.split(',')
		rin.append(float(reg[2]))
		rout.append(float(reg[3]))
		cr.append(-1.0)
		norm2.append(-1.0) # Errors not computed so the values are not included.
		i += 1	
			
	file = open(xspec_file).read()
	cr = re.findall("#Net count rate \(cts/s\) for Spectrum:.+",file)
	cr = [item.lstrip("#Net count rate \(cts/s\) for Spectrum:.+").split()[1] for item in cr]
	stat = re.findall("#### Hey stat \d+\.\d+",file)
	stat = [item.lstrip("#### Hey stat") for item in stat]
	dof = re.findall("#### Hey dof \d+",file)
	dof = [item.lstrip("#### Hey dof") for item in dof]

	# def read_model_params():
	file_array = re.split('\s+',file)

	for index, item in enumerate(file_array):
		if item == "norm":
			if not (float(file_array[index+1]) == 0.01 or float(file_array[index+1]) == 0.001):
				if (int(file_array[index-3]) % PARAMLEN) == 7:
					norm1.append(float(file_array[index+1]))
	
	if len(norm1) % reg_num > 0:
		print("Number of normalization off by {0}").format((len(norm1) % reg_num))
	
	# Usually will fit twice so just grab the last set of fits
	norm1.reverse()
	norm1 = norm1[0:reg_num-1]
		
		
	
	### Extract all of the confidence intervals the same then sort out which type.
	
	# norm error is no longer computed.
	conf_range = re.findall("#\s+\d+\s+\d.+\)",file)
	tx_range = []
	fe_range = []
	norm_range = []
	even_odd = 0
	for item in conf_range:
		conf_range_temp = item.lstrip("#     ")
		conf_range_temp = conf_range_temp.split() 
		# print(int(conf_range_temp[0]) % PARAMLEN)	
		if (int(conf_range_temp[0]) % PARAMLEN) == 2: # T_X conf intervals
			diff = conf_range_temp[3].lstrip("(").split(',')
			t_fit = float(conf_range_temp[1])-float(diff[0])		
			tx_range.append([t_fit,conf_range_temp[1],conf_range_temp[2]]) # [fit, upper lower]
		elif (int(conf_range_temp[0]) % PARAMLEN) == 4 : # Fe conf intervals
			diff = conf_range_temp[3].lstrip("(").split(',')
			fe_fit = float(conf_range_temp[1])-float(diff[0])
			fe_range.append([fe_fit, conf_range_temp[1],conf_range_temp[2]]) # [fit, upper lower]
			if even_odd == 1: # based on our tied bins
				fe_range.append([fe_fit, conf_range_temp[1],conf_range_temp[2]]) # [fit, upper lower]
				even_odd = 0
			even_odd += 1

	# norm conf range not computed
	#	elif (int(conf_range_temp[0]) % PARAMLEN) == 7: # norm conf intervals
	#		diff = conf_range_temp[3].lstrip("(").split(',')
	#		norm_fit = float(conf_range_temp[1])-float(diff[0])
	#		norm_range.append([norm_fit,conf_range_temp[1],conf_range_temp[2]]) # [fit, upper lower]
		else:
			print(str(conf_range_temp) + " didn't work.")
			print("This is parameter #"+str(int(conf_range_temp[0]) % PARAMLEN))
	
# def write_de_file():
#	"""Write de.dat file"""
	out_file = open("/home/Aaron/Xray/outputs/de_"+str(obsid)+"_unbin.dat", "wt")
	fileheader = "##cluster obsid rin rout nh nlo nhi tx tlo thi fe felo fehi norm normlo normhi tx2 tlo2 thi2 norm2 normlo2 normhi2 z cr src cstat dof\n"
	out_file.write(fileheader)
	
	x = 0
	while x < len(tx_range):
		# cluster obsid rin rout nh nlo nhi tx tlo thi fe felo fehi norm normlo normhi tx2 tlo2 thi2 norm2 normlo2 normhi2 z cr src chisq dof
		
		outline = '%s %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f\n' % (name, int(obsid), rin[x], rout[x], nh, nh, nh, float(tx_range[x][0]), float(tx_range[x][1]), float(tx_range[x][2]), float(fe_range[x][0]), float(fe_range[x][1]), float(fe_range[x][2]), norm1[x], norm1[x], norm1[x],TX2,TX2,TX2,norm2[x],norm2[x],norm2[x],z,float(cr[x]),SRC,float(stat[x]),float(dof[x]) )		
		out_file.write(outline) 
		x += 1
			
	out_file.close()
	print("de.dat file created with "+str(x)+" entries")

## Remove line below once it is integrated into the scripts and can be an imported module	
#if __name__ == '__main__':
xspec_extract()





