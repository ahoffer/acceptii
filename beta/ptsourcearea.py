def point_source_area(x_c,y_c,ann_in,ann_out,ptsrc_file,kpc=False):
	"""Use this to figure out the total area of point sources in the annular subtraction
	
	Currently call this function from spectral annulus extraction
	Keyword arguments:
    x_c,y_c -- centroid coordinate
    ann_in,ann_out -- Annuli boundaries
	ptsrc_file -- name of the point source file
	kpc -- use the physical size scale
	
	"""
	
	import os,math
	import numpy as np
	f_ptsrc=open(ptsrc_file,'r') #the point source file
	f_ptsrc.readline() #first line is commented out
	tot=0.0
	PI=3.1415926
	ASEC_PIX = 0.492
	count = 0
	ptsrc = np.zeros((100,3))
	ann_area = 0
	count1 = 0
	while True:
		line = f_ptsrc.readline()
		if len(line)==0:
			break
		line=line.strip('cirle()\n')
		line=line.split(',')
		ptsrc[count,:]=[float(line[0]),float(line[1]),(float(line[2])*ASEC_PIX/60.0)**2*PI] #0.492 "/pixel
		tot=tot+(float(line[2])*ASEC_PIX/60.0)**2*PI  
		count+=1
	ptsrc = ptsrc[0:count-1,:]
	print("Point sources account for "+str(tot)+" square arcminutes")
	f_ptsrc.close()
	ann_area = PI*((ann_out*ASEC_PIX/60.0)**2.0-(ann_in*ASEC_PIX/60.0)**2.0)
	ann_area_original = ann_area
	while count1 < count-1:
		distance = ((x_c-ptsrc[count1,0])**2+(y_c-ptsrc[count1,1])**2)**(0.5)
		if distance <= ann_out and distance >= ann_in:	#distance greater than inner and distance less than outer 
			ann_area = ann_area - ptsrc[count1][2]
		count1 += 1
	print("Original area: " + str(round(ann_area_original,2)) + " Final area: " + str(round(ann_area,2)) + " Lost " + str(round((100*(ann_area_original-ann_area)/ann_area_original),5))+" %.")
	return ann_area
