def blank_sky(newdir=None,clobbber=False):
	"""MODULE: Blank Sky Background creator will merge and reproject.
	http://cxc.harvard.edu/ciao/threads/acisbackground/
	
	Keyword Arguments:
		newdir -- Use to move across directories
		clobber -- Typically you don't want to overwrite files but set this to True to ignore that.
	"""
	import ciao_contrib.runtool as rt
	import os
	import subprocess
	import filefinder
	
	# check to make sure in correct directory and the events file is there
	# should eventually use a dict with the different files types
	print('We are currently in' + os.getcwd())
	if newdir:
		os.chdir(newdir)
		print('We are now in' + os.getcwd())
	
	file_list = filefinder.find_files()
	evt2_file = 'evt2_clean.fits'
	bg_files = {'merge': 'merge_bg.fits','clean': 'merge_bg_clean.fits', 'reproj': 'merge_bg_reproj_clean.fits'}
	
	try:
		print("Looking up ACIS blank sky background files")
		bg_files = rt.acis_bkgrnd_lookup(evt2_file)
		bg_split = bg_files.split()
		bg_list = ','.join(bg_split)
		print(bg_files)
	except:
		print("Couldn't find background files.")
	
	try:
		rt.dmmerge.punlearn()
		rt.dmmerge.infile = bg_list
		rt.dmmerge.outfile = 'merge_bg.fits'
		rt.dmmerge()
		print("Background files merged. Created file merge_bg.fits.")
	except:
		print("Unable to merge background files.")
		
	VFAINT = rt.dmkeypar("evt2_clean-ptsrc.fits","DATAMODE",echo=True)
	if VFAINT == 'VFAINT':
		rt.dmcopy("merge_bg.fits[status=0]","merge_bg_clean.fits")
	else: #just call it the same thing for simplicity. Should probably fix this
		rt.dmcopy("merge_bg.fits","merge_bg_clean.fits")
		
	gain_name_evt2 = rt.dmkeypar("evt2_clean-ptsrc.fits","GAINFILE",echo=True)
	gain_name_bkg = rt.dmkeypar("merge_bg_clean.fits","GAINFILE",echo=True)
	if gain_name_evt2 == gain_name_bkg:
		print("Gain file names are the same. Proceed.")
	else:
		print("ERROR: Gain file names are different. STOP!")
	
	rt.dmmakepar("evt2_clean-ptsrc.fits","event_header.par")
	p,perr = subprocess.Popen(['grep _pnt event_header.par > event_pnt.par'],shell=True,stdout=subprocess.PIPE,stderr=subprocess.PIPE).communicate()
	if perr:
		print('ERROR: '+str(perr))
		
	p,perr = subprocess.Popen(['chmod +w merge_bg_clean.fits'],shell=True,stdout=subprocess.PIPE,stderr=subprocess.PIPE).communicate()
	if perr:
		print('ERROR: '+str(perr))
		
	rt.dmreadpar("event_pnt.par","merge_bg_clean.fits[events]",clobber=True)
	
	rt.reproject_events.punlearn()
	rt.reproject_events.infile = "merge_bg_clean.fits"
	rt.reproject_events.outfile = "merge_bg_reproj_clean.fits"
	rt.reproject_events.aspect = file_list['asolfile']
	rt.reproject_events.match = "evt2_clean.fits"
	rt.reproject_events.random = 0
	rt.reproject_events()
	print("Reprojected background file created.")
	bg_exp = scale_bg()
	print("New background exposure time is: "+str(bg_exp))
	
def scale_bg(energy=[9.5,12.0],ccd_id=7):
	""""Match the high (9.5-12.0 keV) energy count rate between the blank sky background file and the events file. Edit the various time keywords in the background header to compensate.
	
	Keyword Arguments:
	energy -- Set the range of energy to investigate the background.
	ccd_id -- Choose which CCD to use for high energy counts. Can use multiple (e.g. ccd_id = '0:3' if you want the set of four ACIS-I chips.
	"""
	
	import ciao_contrib.runtool as rt
	energy_low = 1000.0*energy[0]
	energy_high = 1000.0*energy[1]
	
	rt.dmstat.punlearn()
	rt.dmstat("evt2_clean-ptsrc.fits[ccd_id="+str(ccd_id)+"][energy=9500:12000][cols energy]", sigma=False,median=False)
	evt_counts = float(rt.dmstat.out_good)
	
	rt.dmstat.punlearn()
	rt.dmstat("merge_bg_reproj_clean.fits[ccd_id="+str(ccd_id)+"][energy=9500:12000][cols energy]", sigma=False,median=False)
	bg_counts = float(rt.dmstat.out_good)
	
	evt_exp = float(rt.dmkeypar("evt2_clean-ptsrc.fits","EXPOSURE",echo=True))
	bg_exp = evt_exp*bg_counts/evt_counts
	print("The new background exposure time is: "+str(bg_exp))
	
	rt.dmhedit("merge_bg_reproj_clean.fits",filelist='none',operation='add',key='EXPOSURE',value=bg_exp)
	rt.dmhedit("merge_bg_reproj_clean.fits",filelist='none',operation='add',key='ONTIME',value=bg_exp)
	rt.dmhedit("merge_bg_reproj_clean.fits",filelist='none',operation='add',key='LIVETIME',value=bg_exp)
	rt.dmhedit("merge_bg_reproj_clean.fits",filelist='none',operation='add',key='TSTOP',value=bg_exp)
	rt.dmhedit("merge_bg_reproj_clean.fits",filelist='none',operation='add',key='TSTART',value=0.0)
	rt.dmhedit("merge_bg_reproj_clean.fits",filelist='none',operation='add',key='DTCOR',value=1.0)
	return bg_exp
