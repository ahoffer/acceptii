FUNCTION  vv, r1, r2
IF (r1 LE r2) THEN return,0.0 ELSE return,4.*!pi/3. * (r1^2 - r2^2)^(1.5)
END

PRO entropy, obsname, accept_pro, modelfit

; set cosmo.
H0 = 70
OM = 0.3
L0 = 0.7

; read in the temperature fits 
restore,"E:\Scripts\SethScripts\xspectemp_rin_normerr_src.sav"
fitdata = read_ascii('E:\Scripts\SethScripts\de_'+obsname+'_unbin.dat',template=xspectemp_rin_normerr_src)


;load the accept data and overplot for comparison

if (accept_pro eq 1) then begin
  accept_prof = read_ascii('E:\Science\Xray\ACCEPT\'+obsname+'_profiles.dat', COMMENT_SYMBOL='#')
  ;accept_prof = read_ascii('E:\Science\Xray\ACCEPT\3595_profiles.dat', COMMENT_SYMBOL='#')
  rin_accept = accept_prof.field01(1,*) ; in Mpc
  rout_accept = accept_prof.field01(2,*) ; in Mpc
  nelec_accept = accept_prof.field01(3,*) ; cm^-3
  rmid_accept=(rin_accept+rout_accept)/2.0 ; Mpc
  nerr_accept = accept_prof.field01(4,*) ; cm^-3
  tx_accept = accept_prof.field01(13,*) ;keV
  txerr_accept = accept_prof.field01(14,*) ;keV
  
endif 


; get list of unique obsids
uidx  = uniq(fitdata.obsid,sort(fitdata.cluster))
name  = fitdata.cluster(uidx)
obsid = fitdata.obsid(uidx)
ncl   = n_elements(name)

; go through each cluster
m=0 & n=ncl
FOR i=m,n-1 DO BEGIN 

    ; get annuli info
    idx2 = where(fitdata.cluster EQ name[i] AND fitdata.obsid EQ obsid[i])

    ; get fit data from xspec
    ; need to skip temperatures that are zero
    
    
    tin   = fitdata.rin[idx2];*60 already in arcseconds
    tout  = fitdata.rout[idx2];*60 already in arcseconds
    tx    = fitdata.tx[idx2]
    tlo   = fitdata.tlo[idx2]
    thi   = fitdata.thi[idx2]
    norm  = fitdata.norm[idx2]
    cr    = fitdata.cr[idx2];should be the net counts in Tx aperture
    z     = fitdata.z[idx2[0]]

    ; get surf brightness profile from CIAO 
    surfdata = mrdfits('E:\Scripts\SethScripts\'+obsname+'\'+obsname+'_rprofile07-2.fits', 1, hdr)
    
    
    
    ;;;;;;;;
    ;EXPERIMENTAL!!!!!
    ; Only look at locations with positive SB values
    ; look like there may be a problem if the surbri profile doesn't extend as far as the temperature profile
    sb     = surfdata.sur_bri  ; units cts/pixel**2
    POSVAL = where(sb gt 0)
    ;print, "WARNING: Some SB elements are negative. They are set to zero."
    
    idx    = sort(surfdata.r[1])
    idx = reverse(idx[POSVAL])
    rin    = surfdata.r[0]
    rin    = rin[idx]*0.492
    rout   = surfdata.r[1]
    rout   = rout[idx]*0.492
    exptime = surfdata.exposure
    exptime = exptime[POSVAL]

    sb     = sb[idx]/(0.492^2.)/exptime ; units cts/arcsec**2/sec
    sberr  = surfdata.sur_bri_err
    sberr  = sberr[idx]/(0.492^2.)/exptime
    sbrint = sb*!pi*(rout^2. - rin^2.) ; sbrint units cts/sec
    nbins  = n_elements(idx) 
    
    
    ; if the surface brightness profile is too noisy use a model instead

if (modelfit eq 1) then begin 
    print, "WARNING: Using a beta model fit instead of the surface brightness profile"
    r_0_model = 302.127
    ampl_model = 0.0422486
    beta_model = 4.0
    rmid_model = (rin + rout)/0.492/2.0
    pix_model = findgen(max(rmid_model))
    exptime_model = exptime[0]
    ;sherpa 1-D beta model for surface brightness fit
    beta_model_fit = ampl_model * (1.0+(pix_model/r_0_model)^2.)^(-3*beta_model+0.5)/0.492^2./exptime_model ;cts/arcsec^2/second
    arcsec_model = pix_model * 0.492

    ;replace sb fits profile with model values 
    sbrint = beta_model_fit * !pi * ((arcsec_model+0.492)^2. - arcsec_model^2.)
    
endif

    ; get Tx and cr/norm ratio to use in each SB bin
    tbin   = dblarr(nbins)
    tbinlo = dblarr(nbins)
    tbinhi = dblarr(nbins)
    kxspec = dblarr(nbins)
    FOR j=0,nbins-1 DO BEGIN 
        tidx = where(rin[j] LT tout AND rin[j] GE tin)
        print, tidx
        IF tidx LT 0 THEN tidx = n_elements(tout)-1
        tbin[j] = tx[tidx[0]]
        tbinlo[j] = tlo[tidx[0]]
        tbinhi[j] = thi[tidx[0]]
        kxspec[j] = norm[tidx[0]]/cr[tidx[0]]
    ENDFOR  

    ; deproject surface brightness to get ne profile  
    cosmology, z, kosmo, /silent
    angtolin = kosmo[4]         ; kpc/arcsec
    toutmpc = tout*angtolin/1000.
    tinmpc  = tin*angtolin/1000.
    tmidmpc = (tinmpc+toutmpc)/2.
    routmpc = rout*angtolin/1000.
    rinmpc  = rin*angtolin/1000.
    ; array of all r values
    vsum = dblarr(nbins,nbins)
    rall = [reform(routmpc),[0]]
    FOR m=0,nbins-1 DO BEGIN 
        FOR j=0,nbins-1 DO BEGIN 
            v1 = vv(rall[j+0], rall[m+1])
            v2 = vv(rall[j+1], rall[m+1])
            v3 = vv(rall[j+0], rall[m+0])
            v4 = vv(rall[j+1], rall[m+0])
            vsum[j,m] = (v1-v2) - (v3-v4)
        ENDFOR  
    ENDFOR 
    cc = dblarr(nbins)
    cc[0] = sbrint[0]/vsum[0,0]
    FOR m=1,nbins-1 DO BEGIN 
        interv = 0.0
        FOR j=0,m-1 DO BEGIN 
            interv = interv + cc[j]*vsum[j,m]
        ENDFOR 
        lastv = vsum[m,m]
	cc[m] = (sbrint[m] - interv)/lastv
    ENDFOR  
    nedivnp = 1.4/1.167         ; fully ionized plasma n_e=1.2n_p
    d = kosmo[3]                ; angular distance (was lum dist)   (should this be angular distance to match the normalization?)
    nelec = sqrt(nedivnp*(kxspec)*cc*4.0*!pi*d^2*(1+z)^2/3.0856e10)
    nerr = sberr/sb*nelec*2.71
    kidx    = where(finite(nelec) EQ 1,nbins)
    nelec   = nelec[kidx]
    nerr    = nerr[kidx]
    routmpc = routmpc[kidx]
    rinmpc  = rinmpc[kidx]
    rmidmpc = (rinmpc+routmpc)/2
    rin     = rin[kidx]
    rout    = rout[kidx]
    rmid    = (rin+rout)/2
    tbin    = tbin[kidx]
    tbinlo  = tbinlo[kidx]
    tbinhi  = tbinhi[kidx]
    sb      = sb[kidx]
    sberr   = sberr[kidx]

    ; convert ne profile to pressure profile
    pp  = tbin*nelec*1.60   ; 1 keV/cm^3 = 1.60d-10 Pascals 
    plo = pp - pp* sqrt( ((tbin-tbinlo)/tbin)^2 + (nerr/nelec)^2)
    phi = pp + pp* sqrt( ((tbinhi-tbin)/tbin)^2 + (nerr/nelec)^2)

    ; convert ne profile to entropy profile
    ss  = tbin/nelec^(2/3.)
    slo = ss - ss* sqrt( ((tbin-tbinlo)/tbin)^2 + 4/9.*(nerr/nelec)^2)
    shi = ss + ss* sqrt( ((tbinhi-tbin)/tbin)^2 + 4/9.*(nerr/nelec)^2)

    ; get mgas profile
    mgas  = dblarr(nbins)
    mgper = 4/3. * !pi * 1.167 * 1.673d-24 * nelec * (routmpc^3-rinmpc^3) * (3.085678d24)^3 / 1.99d33 ; gas mass in solar units
    mgerr = 4/3. * !pi * 1.167 * 1.673d-24 * nerr * (routmpc^3-rinmpc^3) * (3.085678d24)^3 / 1.99d33
    FOR j=0,nbins-1 DO BEGIN 
        mgas[j] = total(mgper[where(rout LE rout[j])])
        mgerr[j] = total(mgerr[where(rout LE rout[j])])
    ENDFOR 

    ; print results to file
    openw,  1, 'E:\Scripts\SethScripts\outputs\'+name[i]+'_'+strcompress(obsid[i],/remove_all)+'_info3.dat'
    printf, 1, format='($,a1)','#'
    printf, 1, ' Name = '+name[i]+', Obsid = '+strtrim(string(format='(a10)',obsid[i]),2)+', z = '+strtrim(string(format='(f10.4)',z),2)
    printf, 1, format='($,a1)','#'
    printf, 1, format='(1a9,15a10)','Rin','Rout','Tx','-err','+err','Ne','Ne_err','P','-err','+err','Mgas','M_err','S','-err','+err'
    printf, 1, format='($,a1)','#'
    printf, 1, format='(1a9,15a10)','["]','["]','[keV]',' ',' ','[cm^-3]',' ','[Pa 1e-10]',' ',' ','[Msun]',' ','[keV cm^2]',' ',' '
    printf, 1, format='($,a1)','#'
    printf, 1, format='(1a9,15a10)','(1)','(2)','(3)','(4)','(5)','(6)','(7)','(8)','(9)','(10)','(11)','(12)','(13)','(14)','(15)'

    FOR jj = 0,n_elements(ss)-1 DO BEGIN  
        printf,1, rin[jj],rout[jj],$
          tbin[jj],tbin[jj]-tbinlo[jj],tbinhi[jj]-tbin[jj],$
          nelec[jj],nerr[jj],$
          pp[jj],pp[jj]-plo[jj],phi[jj]-pp[jj],$
          mgas[jj],mgerr[jj],$
          ss[jj],ss[jj]-slo[jj],shi[jj]-ss[jj], $
          format='(2f10.2,3f10.2,7e10.2,3f10.2)'
    ENDFOR 
    close,1

    ; plot all the profiles
    
    ;plot all in same file different pages (perl script at end doesnt run in windows
    
    
    plotsym, 0, 0.75, /fill
    Set_Plot, 'PS'
    !fancy = 4
    !linetype = 0
    !p.font = 0
    !p.charsize = 0.9
    !p.title = name[i]+' '+strcompress(obsid[i],/remove_all)
    
  ;  device, filename='E:\Scripts\SethScripts\outputs\'+name[i]+'_'+strcompress(obsid[i],/remove_all)+'temp1.ps', /color
    device, filename='E:\Scripts\SethScripts\'+name[i]+'_'+strcompress(obsid[i],/remove_all)+'_unbin_plots.ps', /color

    plot, $
      rmid, sb, $
      /xlog, /ylog, $
      xtitle = textoidl('R_{mid} [arcsec]'), $
      ytitle = textoidl('S_{X} [cts arcsec^{-2}]'), $
      psym = 10, $
      xran = [min(rmid)-0.1*min(rmid), max(rmid)+0.1*max(rmid)], $
      yran = [min(sb(where(sb gt 0)))-0.1*min(sb(where(sb gt 0))),max(sb)+0.1*max(sb)], $
      /xsty, /ysty
    oploterror, rmid, sb, sberr, psym=8
    if modelfit eq 1 then oplot, arcsec_model, beta_model_fit
   ; device,/close

  ;  device, filename='E:\Scripts\SethScripts\outputs\'+name[i]+'_'+strcompress(obsid[i],/remove_all)+'temp11.ps', /color
    plot, $
      tmidmpc, tx, $
      xtitle = textoidl('R_{mid} [Mpc]'), $
      ytitle = textoidl('T_{X} [keV]'), $
      psym = 10, $
      xran = [min(tmidmpc)-0.1*min(tmidmpc), max(tmidmpc)+0.1*max(tmidmpc)], $
      yran = [min(tx)-0.1*min(tx),max(tx)+0.1*max(tx)], $
      ;yran = [0.0,10.0], $
      /xsty, /ysty
    oploterror, tmidmpc, tx, tx-tlo, psym=8, /lobar
    oploterror, tmidmpc, tx, thi-tx, psym=8, /hibar
    if (accept_pro eq 1) then begin
       oploterror, rmid_accept, tx_accept, txerr_accept, psym=5
    endif
    
   ; device, filename='E:\Scripts\SethScripts\outputs\'+name[i]+'_'+strcompress(obsid[i],/remove_all)+'temp2.ps', /color
   ; !p.multi=[0,1,3]
   plotsym, 0, 0.75, /fill
    plot, $
      rmidmpc, nelec, $
      /xlog, /ylog, $
      xtitle = textoidl('R_{mid} [Mpc]'), $
      ytitle = textoidl('n_{elec} [cm^{-3}]'), $
      psym = 10, $
      xran = [min(rmidmpc)-0.1*min(rmidmpc), max(rmidmpc)+0.1*max(rmidmpc)], $
      yran = [min(nelec(where(nelec gt 0)))-0.5*min(nelec(where(nelec gt 0))),max(nelec)+5.0*max(nelec)], $
      /xsty, /ysty
    oploterror, rmidmpc, nelec, nerr, psym=8
    if (accept_pro eq 1) then begin
       oploterror, rmid_accept, nelec_accept, nerr_accept, psym=5
       nelec_interp = interpol(nelec_accept, rmid_accept,rmidmpc)
       ;plot, rmid_accept, nelec/nelec_interp
    endif
    !p.multi=0
   ; device,/close

 ;   device, filename='E:\Scripts\SethScripts\outputs\'+name[i]+'_'+strcompress(obsid[i],/remove_all)+'temp3.ps', /color
    plotsym, 0, 0.75, /fill
    plot, $
      rmidmpc, pp, $
      /xlog, /ylog, $
      xtitle = textoidl('R_{mid} [Mpc]'), $
      ytitle = textoidl('Pressure [10^{-10} Pa]'), $
      psym = 10, $
      xran = [min(rmidmpc)-0.1*min(rmidmpc), max(rmidmpc)+0.1*max(rmidmpc)], $
      yran = [min(pp(where(pp gt 0)))-0.1*min(pp(where(pp gt 0))),max(pp)+0.1*max(pp)], $
      /xsty, /ysty
    oploterror, rmidmpc, pp, pp-plo, psym=8, /lobar
    oploterror, rmidmpc, pp, phi-pp, psym=8, /hibar
   ; device,/close

 ;   device, filename='E:\Scripts\SethScripts\outputs\'+name[i]+'_'+strcompress(obsid[i],/remove_all)+'temp4.ps', /color
    
    plot, $
      rmidmpc, ss, $
      /xlog, /ylog, $
      xtitle = textoidl('R_{mid} [Mpc]'), $
      ytitle = textoidl('K [keV cm^{-2}]'), $
      psym = 10, $
      xran = [min(rmidmpc)-0.1*min(rmidmpc), max(rmidmpc)+0.1*max(rmidmpc)], $
      yran = [min(ss)-0.1*min(ss),max(ss(where(finite(ss) eq 1)))+0.1*max(ss(where(finite(ss) eq 1)))], $
      /xsty, /ysty
    oploterror, rmidmpc, ss, ss-slo, psym=8, /lobar
    oploterror, rmidmpc, ss, shi-ss, psym=8, /hibar
 ;   device, /close

  ;  device, filename='E:\Scripts\SethScripts\outputs\'+name[i]+'_'+strcompress(obsid[i],/remove_all)+'temp5.ps', /color
    plot, $
      rmidmpc, mgas, $
      /xlog, /ylog, $
      xtitle = textoidl('R_{mid} [Mpc]'), $
      ytitle = textoidl('M_{gas} [M_{solar}]'), $
      psym = 10, $
      xran = [min(rmidmpc)-0.1*min(rmidmpc), max(rmidmpc)+0.1*max(rmidmpc)], $
      yran = [min(mgas)-0.1*min(mgas),max(mgas)+0.1*max(mgas)], $
      /xsty, /ysty
    oploterror, rmidmpc, mgas, mgerr, psym=8
   device,/close

    ; make all these ps files into one ps file
    ;SPAWN, 'dir E:\Scripts\SethScripts\outputs\*.ps /b > E:\Scripts\SethScripts\outputs\list.txt'
    ;SPAWN, 'type E:\Scripts\SethScripts\outputs\list.txt | perl E:\Scripts\SethScripts\pscat.pl 6 E:\Scripts\SethScripts\'+name[i]+'_'+strcompress(obsid[i],/remove_all)+'_plots.ps'
    ;SPAWN, 'rm -f temp*.ps'
    ;SPAWN, 'rm -f list'

ENDFOR

close,/all

END
