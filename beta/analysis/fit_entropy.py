# Add ACCEPT profile comparison

def entropy_fit(radius=None, entropy=None, entropy_err=None, guess=None):
	"""
	Fit cluster entropy
	
	Keyword Arguments:
		radius -- Numpy array which contains the radii in kpc
		entropy -- Numpy array of the same length as radius which contains entropy 
	"""
	
	from scipy.optimize import curve_fit
	
	def entr_func(radius,k0,k100,alpha):
		return k0 + k100*(radius/100.)**alpha
		
	fit_param,fit_err = curve_fit(entr_func,radius,entropy,p0=guess,sigma=entropy_err)
		
	return fit_param,fit_err
	
def main():

	import csv
	import numpy as np
	import matplotlib.pyplot as plt
	import matplotlib.gridspec as gridspec
	from matplotlib import rc
	rc('font',**{'family':'sans-serif','sans-serif':['Helvetica']})
	## for Palatino and other serif fonts use:
	#rc('font',**{'family':'serif','serif':['Palatino']})
	rc('text', usetex=True)
	
	import sql_test as sqlt
	
	r_in = np.array([])
	r_out = np.array([])
	Kr = np.array([])
	Kr_err_low = np.array([])
	Kr_err_high = np.array([])
	Tx = np.array([])
	Tx_low = np.array([])
	Tx_high = np.array([])
	nelec = np.array([])
	nelec_err = np.array([])
	P = np.array([])
	P_low = np.array([])
	P_high = np.array([])
	param_start = np.array([])
	
	filename = 'E:/Scripts/SethScripts/outputs/MACSJ1311-03_9381_info3.dat' 
	obsid = '9381'
	# elements start with largest radii and move in, need to convert arcsec to kpc
	file = csv.reader(open(filename,'r'))
	for line in file: 
		if line[0][0] != '#':
			line = line[0].split()
			r_in = np.append(r_in,float(line[0]))
			r_out = np.append(r_out,float(line[1]))
			Tx = np.append(Tx,float(line[2]))
			Tx_low = np.append(Tx_low,float(line[3]))
			Tx_high = np.append(Tx_high,float(line[4]))
			nelec = np.append(nelec,float(line[5]))
			nelec_err = np.append(nelec_err,float(line[6]))
			P = np.append(P,float(line[7]))
			P_low = np.append(P_low,float(line[8]))
			P_high = np.append(P_high,float(line[9]))
			Kr = np.append(Kr,float(line[12]))
			Kr_err_low = np.append(Kr_err_low,float(line[13]))
			Kr_err_high = np.append(Kr_err_high,float(line[14]))

	kpcasec = 2.669 # need to change to calculate based on cosmology later currently kpc/" set for 2014
	r_in = r_in * kpcasec
	r_out = r_out * kpcasec
	r = (r_in + r_out)/2.0
	param_start = np.append(param_start, np.min(Kr))
	
	r_100 = (r/100.0 - 1)**2
	Kr_100 = Kr[r_100 == np.min(r_100)]
	param_start = np.append(param_start, np.min(Kr_100))
	
	param_start = np.append(param_start, 1.2)
	
	param,err = entropy_fit(radius=r, entropy=Kr, entropy_err=Kr_err_low, guess=param_start)
	
	print('K_0 = '+str(param[0])+'+/- '+str(err[0][0]))
	print('K_100 = '+str(param[1])+'+/- '+str(err[1][1]))
	print('alpha = '+str(param[2])+'+/- '+str(err[2][2]))

	fit_r = np.linspace(0.1,np.max(r_out),num=1000)
	fit_y = param[0] + param[1]*(fit_r/100.)**param[2]
	
	fit_r_match = param[0] + param[1]*(r/100.)**param[2]
	fit_resid = Kr - fit_r_match
	
	fit_str = 'K$_0$ =%.2f \n K$_{100}$ = %.2f \n alpha = %.2f'%(param[0],param[1],param[2]) 
	
	gs = gridspec.GridSpec(2, 1,height_ratios=[4,1]) 
	plt.figure()
	plt.subplot(gs[0])
	entropy_plot = plt.errorbar(r, Kr, yerr=[Kr_err_low,Kr_err_high], xerr=[r-r_in,r_out-r],fmt='o',markersize=3)
	entropy_fit_plot = plt.plot(fit_r,fit_y)
	plt.ylabel("Entropy [keV cm$^2$]")
	plt.xscale('log')
	plt.xlim(1,1000)
	plt.yscale('log')
	plt.text(1,100,fit_str,fontsize=10)
	
	plt.subplot(gs[1])
	zero_x = [0.0001,2000]
	zero_y = [0,0]
	entropy_resid_plot = plt.errorbar(r, fit_resid, yerr=[Kr_err_low,Kr_err_high], xerr=[r-r_in,r_out-r],fmt='o',markersize=3)
	horizonal_line = plt.plot(zero_x, zero_y, 'k--')
	plt.xscale('log')
	plt.xlim(1,1000)
	plt.ylabel("Resid")
	plt.xlabel("Distance from centroid [kpc]")

	plt.savefig('E:/Scripts/SethScripts/outputs/entropy_'+obsid+'.pdf',format='pdf')
	
	
	# Plot N_e density, Pressure, Entropy
	plt.figure()

	plt.subplot(221)
	Tx_plot = plt.errorbar(r, Tx, yerr=[Tx_low,Tx_high], xerr=[r-r_in,r_out-r],fmt='o',markersize=3)
	plt.xscale('log')
	plt.xlim(5,1000)
	plt.yscale('log')
	plt.ylabel('Temperature [keV]',fontsize=5)
	

	plt.subplot(222)
	Tx_plot = plt.errorbar(r, nelec, yerr=nelec_err, xerr=[r-r_in,r_out-r],fmt='o',markersize=3)
	plt.xscale('log')
	plt.xlim(5,1000)
	plt.yscale('log')
	plt.ylabel('Electron Density [cm$^{-3}$]',fontsize=5)
	
	
	plt.subplot(223)
	Tx_plot = plt.errorbar(r, P, yerr=[P_low,P_high], xerr=[r-r_in,r_out-r],fmt='o',markersize=3)
	plt.xscale('log')
	plt.xlim(5,1000)
	plt.yscale('log')
	plt.ylabel('Pressure [10$^{-10}$ Pa]',fontsize=5)
	plt.xlabel("Distance from centroid [kpc]")

	
	plt.subplot(224)
	Tx_plot = plt.errorbar(r, Kr, yerr=[Kr_err_low,Kr_err_high], xerr=[r-r_in,r_out-r],fmt='o',markersize=3)
	plt.xscale('log')
	plt.xlim(5,1000)
	plt.yscale('log')
	plt.ylabel("Entropy [keV cm$^2$]",fontsize=5)
	plt.xlabel("Distance from centroid [kpc]")

	plt.savefig('E:/Scripts/SethScripts/outputs/accept_fits_'+obsid+'.pdf',format='pdf')

	# Add the entry to the ACCEPT2 SQL table
	cluster_name = 'MACS1311'
	count = 0
	for line in r_in:
		accept_sql = [cluster_name, count, r_in[count], r_out[count], nelec[count], nelec_err[count], nelec_err[count], Kr[count], Kr_err_low[count], Kr_err_high[count], param[0], param[1], param[2]]
		result = sqlt.insert_accept(accept=accept_sql)
		print(result)
		count += 1
	

if __name__ == "__main__":
    main()