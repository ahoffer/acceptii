####Get lumin from xspec_tx_extraction_tie_unbin

def cooltime(lumin=None,TX=None, norm=None, DA=None, z=None, ne=None, ne_r=None):
	"""Determine the cooling time for a cluster. This is found from the cooling function assumed by MEKAL.
	
	Keyword Arguments:
		lumin -- Bolometric luminosity from Xspec (0.1 - 100 keV) extended from MEKAL
		TX -- X-ray temperature fit from MEKAL model in keV
		norm -- MEKAL model normalization
		DA -- Angular diameter distance from the cosmology in cm
		z -- Redshift of the cluster.
		ne -- Deprojected electron density.
	"""
	import numpy as np
	from scipy.interpolate import interp1d
	import cosmocalc as cc

	# Underscore means / so ERG_KEV is ERG/KEV
	ERG_KEV = 1.60217657E-9
	YEAR_SECONDS = 3.16888e-8
		
	coolfunc =  lumin / (norm*1E14*4*np.pi*(DA*(1.0+z))**2)	#should be around 1e-23

	# we assume n = 2.3nH for fully ionized plasma
	cool_time = 1.5*2.3*TX/(ne*coolfunc) * YEAR_SECONDS * 1E-9 # in Gyr
	
	return cool_time
	
def main():
	"""an example of RXJ2014 running the command"""
	from scipy.interpolate import interp1d
	from cooltime import cooltime
	import matplotlib.pyplot as plt
	import numpy as np

	luminosity = np.array([5.67E+43,7.10E+43,7.51E+43,5.36E+43,8.35E+43,5.95E+43,7.18E+43,8.59E+43,7.88E+43,8.40E+43,9.46E+43,7.72E+43,1.09E+44,9.12E+43,9.88E+43,8.76E+43,8.69E+43,8.38E+43,9.68E+43,8.40E+43,1.15E+44,7.74E+43,7.59E+43,6.91E+43])
	luminosity = luminosity[0:20]

	ne_rin = np.array([250.92,246,241.08,236.16,231.24,226.32,221.4,216.48,211.56,206.64,196.8,186.96,182.04,177.12,172.2,162.36,157.44,152.52,147.6,142.68,137.76,132.84,127.92,123,118.08,113.16,108.24,103.32,98.4,93.48,88.56,83.64,78.72,73.8,68.88,63.96,59.04,54.12,49.2,44.28,39.36,34.44,29.52,24.6,19.68,14.76,9.84,4.92,0])
	ne_rin = ne_rin[::-1]
	ne_rout = np.array([255.84,250.92,246,241.08,236.16,231.24,226.32,221.4,216.48,211.56,201.72,191.88,186.96,182.04,177.12,167.28,162.36,157.44,152.52,147.6,142.68,137.76,132.84,127.92,123,118.08,113.16,108.24,103.32,98.4,93.48,88.56,83.64,78.72,73.8,68.88,63.96,59.04,54.12,49.2,44.28,39.36,34.44,29.52,24.6,19.68,14.76,9.84,4.92])
	ne_rout = ne_rout[::-1]
	ne = np.array([7.06E-04,5.91E-04,3.94E-04,6.09E-04,4.18E-04,4.67E-04,4.96E-04,6.75E-04,4.89E-04,8.61E-04,1.01E-03,7.08E-04,5.15E-04,4.89E-04,7.23E-04,9.12E-04,1.02E-03,8.65E-04,7.46E-04,9.81E-04,5.53E-04,1.40E-03,8.10E-04,1.41E-03,1.10E-03,1.20E-03,1.73E-03,1.67E-03,1.81E-03,1.01E-03,1.93E-03,2.29E-03,2.46E-03,2.70E-03,2.39E-03,3.23E-03,3.44E-03,4.29E-03,5.74E-03,5.32E-03,6.02E-03,6.69E-03,8.63E-03,1.12E-02,1.36E-02,2.05E-02,2.83E-02,5.26E-02,9.41E-02])
	ne = ne[::-1]

	f = interp1d((ne_rin+ne_rout)*0.5,ne,kind='cubic')

	rin = np.array([0,7,11,15,18,23,27,32,39,46,54,65,76,90,104,120,141,165,191,223],dtype=np.float64)
	rout = np.array([7,11,15,18,23,27,32,39,46,54,65,76,90,104,120,141,165,191,223,265],dtype=np.float64)
	ravg = (rin+rout)*0.5

	ne_interp = f(ravg)
	norm = np.array([0.000584,0.000755,0.000826,0.000827,0.000831,0.000742,0.000827,0.000848,0.000826,0.000895,0.00099,0.000799,0.000956,0.000786,0.000814,0.000992,0.000809,0.000686,0.000982,0.000716])
	tx =np.array([3.827204,4.310226,4.790912,5.379089,5.586428,6.204365,5.966215,7.34337,8.83601,7.87644,7.51115,10.21115,8.95185,11.10104,8.52382,8.45794,9.65285,10.69469,8.42666,15.6403])
	da = 1.71440246e27
	red = 0.1555



	times = cooltime(lumin=luminosity,TX=tx,norm=norm,DA=da,z=red,ne=ne_interp)
	print(times)

if __name__ == "__main__":
    main()