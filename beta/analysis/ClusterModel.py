class ClusterModel():
	"""Create and manipulate a simulated cluster"""
	def __init__(self,beta_params=None):
		"""Need to import beta model parameters"""
		# start with a 1-d then allow for 2d sperical
		self.ampl = None
		self.xpos = None
		self.ypos = None
		self.r0 = None
		self.beta = None
		
		
		beta_params = {'ampl': 100., 'xpos': 0., 'ypos': 0, 'r0': 5.53, 'beta': 0.66}
		
		if beta_params:	
			self.ampl = beta_params['ampl'] 
			self.xpos = beta_params['xpos'] 
			self.ypos = beta_params['ypos'] 
			self.r0 = beta_params['r0'] 
			self.beta = beta_params['beta'] 
			
		else:
			self.ampl = float(raw_input("Beta Model Amplitude: "))
			self.xpos = float(raw_input("Beta Model X centroid: "))
			self.ypos = float(raw_input("Beta Model Y centroid: "))
			self.r0 = float(raw_input("Beta Model R0: "))
			self.beta = float(raw_input("Beta Model Beta: "))
			
	#def __del__(self):
	
	def flatten(self,scale=None):
		"""Take 3D model and plot as a 2D image
		
		from Sarazin http://ned.ipac.caltech.edu/level5/March02/Sarazin/Sarazin5_5_1.html we are integrating (summing in this case) over the square to get the observed emission measure. The emission measure beta model is I = B*(1+(r/rc)**2)**(-3*beta+1/2)
		
		Keyword Arguments
			scale -- scale the flattened image such that the brightest pixel is equal to the scale
		"""
		# just arbitrarily choosing direction to sum over
		import numpy as np
		self.flatmodel = np.zeros((self.grid[0],self.grid[1]))
		for z in range(0,self.grid[2]):
			self.flatmodel += self.betamodel[z]**2

		if scale:
			max_pix = np.max(self.flatmodel)
			self.flatmodel = self.flatmodel*scale/max_pix
		
		self.flatmodel = np.random.poisson(self.flatmodel)
		
	def model_3d(self,grid=[512,512,512]):
		"""Create 3D model from Sarazin http://ned.ipac.caltech.edu/level5/March02/Sarazin/Sarazin5_5_1.html
		
		Keyword Arguments:
			grid -- 3 element to give x,y,z grid size. 
		"""
		###from scipy
		import numpy as np
		self.grid = grid
		
		#x = np.ogrid[0:self.grid[0],0:self.grid[1],0:self.grid[2]]
		z,y,x = np.mgrid[0:self.grid[0],0:self.grid[1],0:self.grid[2]]
		radius = ((z - self.grid[2]/2)**2 + (y - self.grid[1]/2)**2 + (x - self.grid[0]/2)**2)**0.5
		
		self.betamodel = self.ampl*(1+((radius)/self.r0)**2)**(-1.5*self.beta)
		#self.betamodel = self.ampl*(1+((x - self.grid[2]/2)/self.r0)**2)**(-1.5*self.beta)
		#self.betamodel = self.ampl*(1+np.absolute(x - self.grid[2]/2))**(-1.5*self.beta) #place the center of the cluster at the center of the box
		
	def add_ptsrc(self,num_ptsrc=0):
		if num_ptsrc:
			print("Insert point source")
			# determine point source location
			# convolve with chandra psf?
		else:
			print("There are no point sources to add")
	def add_cavity(self,num_cavity=1,size=10,from_center = [10,0,0]):
		"""Add 2 symmetric bubble near the center of the cluster in 3D
		
		Keyword Arguments:
			num_cavity -- Number of cavities to include
			size -- what is the radius of the cavity
			from_center -- How far is it offcenter
		"""
		import numpy as np
		z,y,x = np.ogrid[0:self.grid[0],0:self.grid[1],0:self.grid[2]]
		mask = ((z - self.grid[2]/2 - from_center[2])**2 + (y - self.grid[0]/2 - from_center[1])**2 + (x - self.grid[1]/2 - from_center[0] )**2) > size**2

		neg_mask = ((z - self.grid[2]/2 + from_center[2])**2 + (y - self.grid[0]/2 + from_center[1])**2 + (x - self.grid[1]/2 + from_center[0] )**2) > size**2
		
		self.betamodel = self.betamodel*mask*neg_mask
		
		#return self.betamodel*mask
		print("Cavity added. Make sure to run flatten if you are plotting the 2D image.")
		#self.betamodel modify to remove cavity from above 
			
	def plot_3d(self):
		"""Plot the 3D cluster """
		from mpl_toolkits.mplot3d import Axes3D
		from pylab import figure
		fig = figure(1)
		ax = fig.add_subplot(1,1,1,projection='3d')
		ax.scatter(self.betamodel[:,0,0],self.betamodel[:,0,0],self.betamodel[:,0,0],c=self.betamodel[:,:,:]/self.ampl*255.,s=10)
	
	def plot_2d(self,outfile='E:/Science/cluster_2d.fits'):
		"""Plot the simlulated cluster image to a fits file. Must be done after model_3D and flatten.

		Keyword Arguments:
			outfile -- Full directory name of the outputted fits file.
		"""
		import pyfits
		import numpy as np
		
		try:
			hdu = pyfits.PrimaryHDU(self.flatmodel)
			hdulist = pyfits.HDUList([hdu])
			hdulist.writeto(outfile, clobber=True)
			# Add data to header
			
			
		except AttributeError:
			print("Did not work. Going to try to flatten first")
			self.flatten()
			self.plot_2d()
			
	def sur_bri(self,ann_space=5,ann_range=[0,200], deg = [0,30]):
		"""Extract surface brightness profile from the flattened model.
		
		Keyword Arguments:
			ann_space -- Annulus width in pixels
			ann_range -- Minimum and maximum pixels for the annular spectra(need to make sure the range is within the max of the grid size)
			deg -- Annulus wedge angle in degrees
			
		
		"""
		
		import numpy as np
		
		y,x = np.ogrid[0:self.grid[0],0:self.grid[1]]
		y = y - self.grid[1]/2
		x = x - self.grid[0]/2
		
		ann_num = (ann_range[1] - ann_range[0])/ann_space
		ann_in = np.linspace(ann_range[0], ann_range[1] - ann_space, ann_num)
		ann_out = np.linspace(ann_range[0] + ann_space, ann_range[1], ann_num)
		if ann_range[1] > np.max(self.grid)/2:
			print("Range too large. Set to half the grid size.")
			ann_range[1] = np.max(self.grid)/2
		
		sb = np.array([])
		sb_err = np.array([])
		radians = np.array(deg) * np.pi/180
		
		for ann in np.transpose(np.vstack((ann_in,ann_out))):
			####### need to work with the angles not quite right
			#ann_mask = [((y**2 + x**2) >= ann[0]**2) & ((y**2 + x**2) < ann[1]**2) & ((y**2 + x**2)**0.5*np.cos(radians[0]) > x) & ((y**2 + x**2)**0.5*np.sin(radians[0]) > y) & ((y**2 + x**2)**0.5*np.sin(radians[1]) < y) & ((y**2 + x**2)**0.5*np.cos(radians[1]) < x)]
			########################
			ann_mask = [((y**2 + x**2) >= ann[0]**2) & ((y**2 + x**2) < ann[1]**2)]
			ann_area = np.pi*(ann[1]**2 - ann[0]**2)
			region = ann_mask * self.flatmodel
			region = region.astype(np.float64)
			counts = region[region > 0]
			ann_error = np.sum(np.sqrt(counts))/ann_area
			sb_err = np.append(sb_err, ann_error)
			ann_sb = np.sum(counts)/ann_area
			sb = np.append(sb, ann_sb)
			
			# need to fix the error term because it is zero. Probably need to do the stdev over the all the pixels?
		
		return [sb,sb_err,np.vstack((ann_in,ann_out))]
			
			
	def singlebeta(radius=None, sb=None, sb_err=None, guess=None):
		"""
		Fit cluster surface brightness with single beta model
		
		Keyword Arguments:
			radius -- Numpy array which contains the radii in same units as guesses
			sb -- Numpy array of the same length as radius which contains sb 
			sb_err -- Numpy array of the same length as radius which contains sb_err ***ALL ELEMENTS MUST BE NON-ZERO
			guess -- Guess for the ampl,r0, beta, xpos parameters
		"""
		
		from scipy.optimize import curve_fit
		
		# need to make sure no error = 0, otherwise it will always return an error
		if sb_err[sb_err == 0]:
			print("Warning: There is an element with zero error.")
			#temporary fix
			sb_err[sb_err == 0] = 999.0
		
		def entr_func(radius,ampl,r0,beta,xpos):
			return ampl*(1+(radius-xpos/r0)**2)**(-3*beta+0.5)
			
		fit_param,fit_err = curve_fit(entr_func,radius,sb,p0=guess,sigma=sb_err)
			
		return fit_param,fit_err		
		
		