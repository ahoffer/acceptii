def point_source(ptfile=False):  
	"""MODULE: Point source use to extract point sources, can give it a file or use wavdetect
	
	Keyword arguments:
	ptfile -- list the full point source file
	"""
	print("Point source module")
	if ptFile:
		import csv
		import numpy as np
		ptsrc = np.zeros((100,3))
		PI = 3.1415926
		# Must be in CIAO region format, circles for shapes, and skip commented lines.
		f_ptsrc = open(ptFile,'r')
		f_ptsrc.readline() # First line is commented out
		while True:
			line = f_ptsrc.readline()
			if len(line)==0:
				break
			line = line.strip('cirle()\n')
			line = line.split(',')
			ptsrc[count,:] = [float(line[0]),float(line[1]),(float(line[2])*0.492/60.0)**2*PI] #0.492 "/pixel
				
	else:
  		wavdetect.punlearn()
		wavdetect.infile = evt2file
		wavdetect.outfile = evt2-ptsrc
		wavdetect.scellfile = "scell.fits"
		wavdetect.imagefile = "imgfile.fits"
		wavdetect.defnbkgfile = "nbgd.fits"
		wavdetect.regfile = "pt_src.reg"
		wavdetect()
	
	return ptsrc
	
	
	def view_regions():
		"""Open DS9 window to display where point sources were extracted"""
	
