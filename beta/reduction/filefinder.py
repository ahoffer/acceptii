def find_files():
	"""Will find all of the standard reprocessed files needed for the modules"""
	import glob
	import os
	
	print('Finding standard reprocessed files in the current directory')
	
	file_dict ={}
	file_dict['directory'] = os.getcwd()
	file_dict['mskfile'] = glob.glob('acis*msk1.fits')[0]
	file_dict['badpixfile'] = glob.glob('acis*bpix1.fits')[0]
	file_dict['origevents'] = glob.glob('acis*evt2.fits')[0]
	file_dict['pbkfile'] = glob.glob('acis*pbk0.fits')[0]
	file_dict['asolfile'] = glob.glob('pcadf*asol1.fits')[0]
	return file_dict