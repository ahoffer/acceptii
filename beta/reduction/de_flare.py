def deflare():
	"""Run the deflare routine to clean background flares.
	http://cxc.harvard.edu/ciao/threads/flare/
	"""
	import ciao_contrib.runtool as rt
	import lightcurves
	import filefinder
	
	print("Cleaning background flares and filtering good time interval")
	file_list = filefinder.find_files()
	
	rt.dmcopy(file_list['origevents']+'[energy=500:7000]',"evt2_05-7.fits")

	#create region to excise cluster
	rt.dmcopy("evt2.fits[exclude sky=region(sources.reg)]", "evt2-sources.fits")
	
	# Need to get filtering working otherwise just look at whole time not GTI
	rt.dmlist.punlearn()
	count = 0
	gti = []
	while count < 8:
		try:
			gti.append(rt.dmlist("evt2_clean.fits[GTI"+str(count)+"]","data"))
		except:
			print("No data in GTI"+str(count))
		count += 1
	
	rt.dmextract.punlearn()
	#rt.dmextract.infile = "evt2-sources.fits[bin time="+str(gti1)+":"+str(gti2)+":259.28]"
	rt.dmextract.infile = "evt2-sources.fits[bin time=::259.28]"
	rt.dmextract.outfile = "evt2-sources.lc"
	rt.dmextract.opt = "ltc1"
	rt.dmextract()
	
	lightcurves.lc_clean("evt2-sources.lc", outfile="evt2-sources.gti")
	dmcopy("evt2-ptsrc.fits[@evt2-sources.gti]",outfile="evt2-ptsrc_clean.fits")
	
	filt_exp = float(rt.dmkeypar("evt2-ptsrc_clean.fits", "EXPOSURE", echo=True))
	unfilt_exp = float(rt.dmkeypar("evt2-sources.fits", "EXPOSURE", echo=True))
	
	print("Unfiltered exposure time is: "+str(unfilt_exp)+" ks.")
	print("Filtered exposure time is: "+str(filt_exp)+" ks.")
	print("Lost "+str(unfilt_exp - filt_exp)+" ks to flares.")
	