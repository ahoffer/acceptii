#extract fit from Xspec fit
import math,os,matplotlib
f=open('E:\\Science\\Xray\\2320\\repro\\xspec_tx_fit_2320.log','r') #xspec output
count=0
anncount=0
obsid = '2320'
asec_pix = 0.492 # "/pix
kpc_asec = 3.098 # kpc/"
tx = []
fe = []
z = []
stat = []
dof = []
ann = []
ann_num = []
while True:
	line = f.readline()
	if len(line)==0:
		print("EOF")
		break
	#if "Spectral Data File" in line:
	#	line=line.split(' ')
	#	ann_name = line[3]
	#	ann_name=ann_name.strip('.pi')
	#	ann.append(ann_name)
	#	ann_num.append(anncount)
	#	anncount=anncount+1
	if "#Hey" in line:
		line=line.strip('#Hey\n')
		line=line.split(' ')
		if "tx" in line:
			tx.append(float(line[2]))
		if "fe" in line:
			fe.append(float(line[2]))
		if "z" in line:
			z.append(float(line[2]))
		if "stat" in line:
			stat.append(float(line[2]))
		if "dof" in line:
			dof.append(float(line[2]))
	count+=1
redchi=np.divide(stat,dof) #reduced chisq

while anncount <= len(tx)-1:
	regname = 'E:\\Science\\Xray\\2320\\repro\\annulus'+str(anncount)+'.reg'
	freq = open(regname,'r')
	line_reg = freq.readline()
	line_reg = line_reg.strip("annulus()\n")
	line_reg = line_reg.split(',')
	#ann.append([float(line_reg[2]),float(line_reg[3])])
	ann.append(float(line_reg[2]))
	anncount+=1

ann = np.multiply(ann,asec_pix)
ann = np.multiply(ann,kpc_asec)
#temperature profile
temp = subplot(111)
temp.plot(ann,tx,'o')
temp.set_ylim(0,10)
temp.set_xlabel("R [kpc]")
temp.set_ylabel("Temperature [keV]")
temp.set_title("Temperature Profile")
savefig('E:\\Scripts\\temp_'+obsid+'.pdf',transparent=True)
#metallicity profile
met=subplot(211)
met.plot(ann,fe,'o')
met.set_xlabel("R [kpc]")
met.set_ylabel("Metallicity [Z/Z_sun]")
met.set_title("Metallicity Profile")
savefig('E:\\Scripts\\met_'+obsid+'.pdf',transparent=True)