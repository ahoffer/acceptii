#!/usr/bin/python
# open up xspec log files and get the relevant fits and errors

import re
import os
import logging
# create a file cluster.log which captures all the information kept constant and given below
# cluster_file = 'E:/Scripts/SethScripts/clusterdata.log' 
# exec(compile(open("E:/Scripts/xspec_tx_extraction.py").read(), "E:/Scripts/xspec_tx_extraction.py", 'exec'))
# INPUTS
##################################################################

name=''


print("Cluster Name: ")
name = raw_input()
print("Obsid: ")
obsid = int(raw_input())
print("Redshift: ")
z = float(raw_input())
print("nH (10^20 cm^-2): ")
nh = float(raw_input())
print("Xspec full file path: ")
xspec_file = raw_input()
print("Number of regions: ")
reg_num = int(raw_input())
print("Region prefix full path: ")
reg_prefix = raw_input()
print("Region suffix: ")
reg_suffix = raw_input()

tx2 = 0.6 #temperature of soft xray bg fixed based on Ken's thesis
norm2 = [] #normalization computed and allowed to be negative
cr = [] #count rate in cts/second based on counts in the 0.5-2 keV range given in the SB profile (XSPEC VALUES?)
rin = [] # inner radius for annuli
rout = [] # outer radius for annuli
src = 1
#################################################################

i=0
#import region file to get the inner and outer radii
while(i < reg_num):
	reg_file = reg_prefix+str(i)+reg_suffix
	reg = open(reg_file).readline()
	reg = reg.strip('anlus()\n')
	reg = reg.split(',')
	rin.append(float(reg[2]))
	rout.append(float(reg[3]))
	cr.append(1.0)
	norm2.append(1.0)
	i+=1
	
	
file = open(xspec_file).read()

#find the fits and strip the line for the number
tx = re.findall("##### Hey tx \d+\.\d+",file)
tx = [item.lstrip("#### Hey tx") for item in tx]
fe = re.findall("#### Hey fe \d*\.*\d+",file)
fe = [item.lstrip("#### Hey fe") for item in fe]
stat = re.findall("#### Hey stat \d+\.\d+",file)
stat = [item.lstrip("#### Hey stat") for item in stat]
dof = re.findall("#### Hey dof \d+",file)
dof = [item.lstrip("#### Hey dof") for item in dof]
#see while loop below for treatment of norm
#norm = re.findall("#### Hey norm \d+",file)
#norm = [item.lstrip("#### Hey norm") for item in norm]
cr = re.findall("#Net count rate \(cts/s\) for Spectrum:.+",file)
cr = [item.lstrip("#Net count rate \(cts/s\) for Spectrum:.+").split()[1] for item in cr]
#find the confidence intervals for the parameters
tx_range = re.findall("#\s+2\s+\d.+\)",file)
#tx_range = [item.lstrip("#     2      ") for item in tx_range]
fe_range = re.findall("#\s+4\s+\d.+\)",file)
#fe_range = [item.lstrip("#     4      ") for item in fe_range]
norm_range = re.findall("#\s+7\s+\d.+\)",file)
#norm_range = [item.lstrip("#     7      ") for item in norm_range]

linenum = len(tx)
out_file = open("E:/Scripts/SethScripts/de_"+str(obsid)+"_unbin.dat", "wt")
fileheader = "##cluster obsid rin rout nh nlo nhi tx tlo thi fe felo fehi norm normlo normhi tx2 tlo2 thi2 norm2 normlo2 normhi2 z cr src chisq dof"
out_file.write(fileheader)

x=0
while x < linenum:
	tx_range_temp = tx_range[x].split()
	tx_hi = float(tx_range_temp[1])
	tx_lo = float(tx_range_temp[0])
	fe_range_temp = fe_range[x].split()
	fe_hi = float(fe_range_temp[1])
	fe_lo = float(fe_range_temp[0])
	norm_range_temp = norm_range[x].split()
	norm_hi = float(norm_range_temp[1])
	norm_lo = float(norm_range_temp[0])
	#temporary normalization fix using the confidence interval because there is no "Hey norm" line currently
	norm_conf = norm_range_temp[2].strip('\)\(').split(',')
	norm = norm_lo - float(norm_conf[0])
	#cluster obsid rin rout nh nlo nhi tx tlo thi fe felo fehi norm normlo normhi tx2 tlo2 thi2 norm2 normlo2 normhi2 z cr src chisq dof
	outline = '%s %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f\n' % (name, obsid, rin[x], rout[x], nh, nh, nh, float(tx[x]), tx_lo, tx_hi, float(fe[x]), fe_lo, fe_hi, norm, norm_lo, norm_hi, tx2,tx2,tx2,norm2[x],norm2[x],norm2[x],z,float(cr[x]),src,float(stat[x]),float(dof[x]) )
	out_file.write(outline) 
	x+=1
	
out_file.close()


