def acceptII():
	"""Main script for running the ACCEPT II modules

	#EXECUTE THE FILE in python3.0 exec(compile(open("acceptii.py").read(), "acceptii.py", 'exec'))
	http://cxc.harvard.edu/ciao/scripting/runtool.html

	List of Available Modules:
	Reprocess - Download and reprocess Chandra data
	Point Source Extraction - extract point sources using wavdetect or hand input regions
	Surface Brightness Profile - Adaptive Bin, Constant Bin

	"""

	### use subprocess to run command line through python http://cxc.harvard.edu/ciao4.4/scripting/runtool.html#subprocess

	### CHECK TO SEE IF CIAO IS OPEN. IF NOT, START IT WITH SUBPROCESS.

	from ciao_contrib.runtool import *  ### change to "import ciao_contrib.runtool as ct" and change all CIAO functions below
	import os
	import subprocess
	import re
	import logging
	from cosmocalc import cosmocalc
	import csv
	import numpy as np
	
	
	### Importing user generate modules
	import Cluster
	from point_source import point_source
	from twod import two_d
	
	
	
	
	##################
	#decide on method to name clusters
		theClusters = []
		theClusters.append(myCluster, myCluster2)

		myCluster1 = Cluster()
		myCluster2 = Cluster()
	
		theClusters[0].name
		theClusters[1].name
		clusters['name1'].name
	
	##### Need to figure out how to do logging	
	logging.basicConfig(filename='acceptii.log', filemode='w', level=logging.DEBUG)

	# Define dictionaries for standards
	astro_scalings = {'arcsec_arcmin':60.0}
	cosmology = {'H0':70.0,'omegaL':0.7,'omegaM':0.3}
	chandra_scaling = {'arcsec_pixel':0.492}
	directories = {'repro': 'repro', 'images': 'images', 'spectra': 'spectra']
	fileprefixes = {'evt2': 'evt2','back': 'merge_bg.fits','back_reproj':'merge_bg_reproj.fits'}

	def download_obsid(obsid=None):
		""""Download the obsid from Chaser
		http://cxc.harvard.edu/ciao/threads/archivedownload/
		"""
		import subprocess
		if not obsid:
				
		DOWNLOAD = "download_chandra_obsid "+str(obsid)
		p = subprocess.Popen(["download_chandra_obsid",str(obsid)],shell=True,stdout=subprocess.PIPE,stderr=subprocess.PIPE)
		(sout,serr) = p.communicate()
		print(sout)
		return print("####OBSID "+obsid+" has been downloaded\n\n")


	curdir = os.getcwd()
	print("Your current working directory is " + curdir)
	### Define the starting files here 
	### link to dictionary later
	directory = ' '.join(os.listdir('.')) 
	badpixfile = re.findall("\w+bpix1.fits",directory)
	evt2file = re.findall("\w+evt2.fits",directory)
	evt2ptsrc = "evt2-ptsrc.fits"

	def find_files():
		"""Will find all of the standard reprocessed files needed for the modules"""
		import glob
		import os
		file_dict ={}
		file_dict['directory'] = os.getcwd()
		file_dict['mskfile'] = glob.glob('acis*msk1.fits')[0]
		file_dict['badpixfile'] = glob.glob('acis*bpix1.fits')[0]
		file_dict['origevents'] = glob.glob('acis*evt2.fits')[0]
		file_dict['pbkfile'] = glob.glob('acis*pbk0.fits')[0]
		file_dict['asolfile'] = glob.glob('pcadf*asol1.fits')[0]
		return file_dict
	centroid = determine_centroid(ui=True)


	def repro():
		"""MODULE: Reprocess. used to reprocess recently downloaded data """

		### need to figure out checks to be made (directory and settings)
		chandra_repro.root()
		chandra_repro
		### Change to the reprocessed files directory 
		os.chdir('repro')


		
	ardlib.punlearn()
	acis_set_ardlib(badpixfile)

	def deflare():
		"""Run the deflare routine"""
		
		dmlist.infile = 'evt2-sources.fits[GTI0]'
		dmlist.opt = 'data'
		dmlist()
		dmlist.infile = 'evt2-sources.fits[GTI1]'
		dmlist.infile = 'evt2-sources.fits[GTI2]'
		dmlist.infile = 'evt2-sources.fits[GTI3]'
		dmlist.infile = 'evt2-sources.fits[GTI6]'
		dmlist.infile = 'evt2-sources.fits[GTI7]'
		
		### extract GTI 
		
		dmextract.punlearn()
		dmextract.infile = "evt2-sources.fits[bin time="+str(gti1)+":"+str(gti2)+":259.28]"
		dmextract.outfile = "evt2-source.lc"
		dmextract.opt = "ltc1"
		dmextract()
		
		import lightcurves
		lightcurves.lc_clean("evt2-sources.lc", outfile="evt2-sources.gti")
		dmcopy("evt2-ptsrc.fits[@evt2-sources.gti]",outfile="evt2-ptsrc_clean.fits")

	
	def make_sb_profile(): 
		"""Module: Create surface brightness profile"""
		import numpy as np
		import matplotlib.pyplot as plt

		# data
		sb_annulus = [[0,1],[1,2]]
		sb_counts = [2,4]
		sb_counts_err = [0.2,0.4]

		# http://matplotlib.org/examples/pylab_examples/errorbar_demo.html
		fig, axs = plt.subplots(nrows=2, ncols=2, sharex=True)
		ax = axs[0,0]
		ax.errorbar(x, y, yerr=yerr, fmt='o')
		ax.set_title('Vert. symmetric')
		ax = axs[1,1]
		ax.set_yscale('log')
		# Here we have to be careful to keep all y values positive:
		ylower = np.maximum(1e-2, y - yerr)
		yerr_lower = y - ylower

def main():
	acceptII()
		
if __name__ == "__main__":
	main()



	

