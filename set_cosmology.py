Class set_cosmology(): # turn this into an object?
	"""Allow user to set their own cosmology parameters"""
	
	H0 = 70.0 
	omegaM = 0.3
	omegaL = 0.7
	
	print('What parameter would you like to change?')	
		
	def print_cosmology():
		return('Cosmology is currently set to H_0 = {0}, omegaM = {1}, omegaL = {2}'.format(H0,omegaM,omegaL))
		
	def set_cosmology():
		if (omegaM+omegaL) != 1:
			print("Selected cosmology is not flat!")
	
