# -*- coding: utf-8 -*-
# <nbformat>2</nbformat>

# <codecell>

try:
    import scipy.stats as stats
except ImportError:
    print("Couldn't import scipy.stats.")
try:
    import pyfits
except ImportError:
    print("Couldn't import pyfits.")
try:
    import numpy as np
except ImportError:
    print("Couldn't import numpy.")
try:
    import matplotlib.pyplot as plt 
except ImportError:
    print("Couldn't import matplotlib.pyplot.")
try:
    import pyregion
except ImportError:
    print("Couldn't import pyregion.")
try:
    from astroML.plotting import hist as astrohist
except ImportError:
    print("Couldn't import astroML.plotting.")

# <codecell>

"""Will eventually be parameters in a function."""
fits_list = ['E:/Science/OBS/2013-04-06/mM87_V0.242.fits',
             'E:/Science/OBS/2013-04-06/mM87_V1.243.fits',
             'E:/Science/OBS/2013-04-06/mM87_V3.244.fits',
             'E:/Science/OBS/2013-04-06/mM87_V5.245.fits']
# Needs the newline after image to work properly.
source_region = """image 
box(1690.0145,728.99858,11.036215,9.690335,0)"""

#Make sure that if you use an annulus is isn't in a chip gap.
bg_region = """image 
box(1669.9351,732.74471,15,8,24.999994)"""

log_file = 'E:/Documents/aastex52/polarization/polM87_jet.log'

# <codecell>

fits_file_0 = fits_list[0]
#header = fits_image[0].header # header is a python dictionary
fits_image_0 = pyfits.open(fits_file_0)
fits_data_0 = fits_image_0[0].data # Make sure to grab the right FITS extension
fits_data_0 = fits_data_0.astype('float')

fits_file_1 = fits_list[1]
#header = fits_image[0].header # header is a python dictionary
fits_image_1 = pyfits.open(fits_file_1)
fits_data_1 = fits_image_1[0].data # Make sure to grab the right FITS extension
fits_data_1 = fits_data_1.astype('float')

fits_file_3 = fits_list[2]
#header = fits_image[0].header # header is a python dictionary
fits_image_3 = pyfits.open(fits_file_3)
fits_data_3 = fits_image_3[0].data # Make sure to grab the right FITS extension
fits_data_3 = fits_data_3.astype('float')

fits_file_5 = fits_list[3]
#header = fits_image[0].header # header is a python dictionary
fits_image_5 = pyfits.open(fits_file_5)
fits_data_5 = fits_image_5[0].data # Make sure to grab the right FITS extension
fits_data_5 = fits_data_5.astype('float')

"""Take random region shape out of image"""

#x,y are flipped in pyfits arrays so NAXIS2,NAXIS1
shape = (fits_image_5[0].header["NAXIS2"], fits_image_5[0].header["NAXIS1"])

region_string = source_region
r0 = pyregion.parse(region_string)
m = r0.get_mask(shape=shape)

bg_region_string = bg_region
r_bg = pyregion.parse(bg_region_string)
m_bg = r_bg.get_mask(shape=shape)

#test to make sure the region is correct location
filt_image = m*fits_image_5[0].data
hdu = pyfits.PrimaryHDU(filt_image)
hdulist = pyfits.HDUList([hdu])
hdulist.writeto('E:/masked_region.fits',clobber=True)

# <codecell>

"""Bin the image. Will need to work out stokes params
on image instead of single value from regions
"""
#currently not working in this form to bin the whole image
#num_bins = 2.0
#H0 = np.histogram2d(fits_data_0[1,:],fits_data_0[:,0],bins=(num_bins,num_bins))

print(len(r_bg[0].coord_list))

# <codecell>

PI = np.pi


"""Will need to fix. Assumes coord_list is either  a circle or a box
to be used to compute the area of  the background."""

if len(r_bg[0].coord_list) == 3:
    bg_radius = r_bg[0].coord_list[2]
    bg_area = PI*bg_radius**2
if len(r_bg[0].coord_list) == 5:
    bg_area = r_bg[0].coord_list[2]*r_bg[0].coord_list[3]
    print("bg is a box")
else:
    bg_area = 0
    print("""WARNING: Background is not in a recognized string format!
          must be either a circle or a box""")
  
    
    

bg_0 = m_bg*fits_data_0
bg_1 = m_bg*fits_data_1
bg_3 = m_bg*fits_data_3
bg_5 = m_bg*fits_data_5
#bg_0[bg_mask] = 0
#bg_1[bg_mask] = 0
#bg_3[bg_mask] = 0
#bg_5[bg_mask] = 0

print(bg_area,np.sum(m_bg))

bg_pix_0 = np.sum(bg_0)/bg_area
bg_pix_1 = np.sum(bg_1)/bg_area
bg_pix_3 = np.sum(bg_3)/bg_area
bg_pix_5 = np.sum(bg_5)/bg_area

fits_data_0 = m*fits_data_0
fits_data_1 = m*fits_data_1
fits_data_3 = m*fits_data_3
fits_data_5 = m*fits_data_5

print("Background subtracted")
fits_data_0 = fits_data_0[fits_data_0 != 0]-bg_pix_0
fits_data_1 = fits_data_1[fits_data_1 != 0]-bg_pix_1
fits_data_3 = fits_data_3[fits_data_3 != 0]-bg_pix_3
fits_data_5 = fits_data_5[fits_data_5 != 0]-bg_pix_5

#need to test sometimes it is off by one pixel and it throws and error
dim = np.shape(fits_data_0)
dim_test = np.shape(fits_data_1)

print("Dimension is "+str(dim)+','+str(dim_test))
print("Number of pixels is "+str(np.sum(m)))
print("Should be the same.")

# factor of 2 because 2 e-/DN
im0 = 2*np.reshape(fits_data_0, dim)
im1 = 2*np.reshape(fits_data_1, dim)
im3 = 2*np.reshape(fits_data_3, dim)
im5 = 2*np.reshape(fits_data_5, dim)
print("Converted to electrons")

# <codecell>

np.shape(fits_data_0)

# <codecell>

np.std(bg_0[bg_0 > 0]),np.mean(bg_0[bg_0 > 0])

# <codecell>

"""Calculate Stokes Parameters"""
val0 = np.sum(im0)
val1 = np.sum(im1)
val3 = np.sum(im3)
val5 = np.sum(im5)

#compute polarization at every pixel
#val0 = im0
#val1 = im1
#val3 = im3
#val5 = im5

im0_err = np.sqrt(val0 + dim[0]**2*np.std(bg_0[bg_0 > 0])**2/bg_area)
im1_err = np.sqrt(val1 + dim[0]**2*np.std(bg_1[bg_1 > 0])**2/bg_area)
im3_err = np.sqrt(val3 + dim[0]**2*np.std(bg_3[bg_3 > 0])**2/bg_area)
im5_err = np.sqrt(val5 + dim[0]**2*np.std(bg_5[bg_5 > 0])**2/bg_area)
I01 = (val0 + val1)/2.
I35 = (val3 + val5)/2.
Iavg = (I01 + I35)/2.
Q = (val0-val1)/2.
U = (val3-val5)/2.
V = ((Q**2.+U**2.)/Iavg**2.)**0.5

# need to change angle range because it is only reported from 0-45
# conditions based on whether U and Q are + or -
polangle = 0.5*np.arctan(U/Q) 

#should verify the error propogation is correct
polangle_err = 0.5/Q/(1+(U/Q)**2)*np.sqrt(Q_err**2*(U/Q)**2+U_err**2)
# convert the angles such that they are all between 0-180

#between 45 - 90
if (Q < 0 and U > 0):
    polangle = polangle + PI/2.
# between 90 - 135
if (Q < 0 and U < 0):
    polangle = polangle + PI/2.
#between 135 - 180
if (Q > 0 and U < 0):
    polangle = polangle + PI
    
I01_err = 0.5*(im0_err**2.+im1_err**2.)**0.5
I35_err = 0.5*(im3_err**2.+im5_err**2.)**0.5
Iavg_err = 0.5*(I01_err**2.+I35_err**2.)**0.5

Q_err = I01_err
U_err = I35_err
Vunbias = ((Q**2.+U**2.)**0.5 - (Q_err**2 + U_err**2)**0.5)/np.abs(Iavg)
V_err = ((Q*Q_err)**2.+(U*U_err)**2.+(Iavg_err/Iavg)**2.)**0.5/(V*Iavg**2.)
print("Iavg = "+str(np.round(Iavg,0))+" I01 = "+str(np.round(I01/Iavg,3))+"Iavg I35 = "+str(np.round(I35/Iavg,3))+"Iavg")
print("Q = "+str(Q)+" +/- "+str(Q_err))
print("U = "+str(U)+" +/- "+str(U_err))
print("V = "+str(np.round(V*100.,2))+"% +/- "+str(np.round(V_err*100,4)))+"%"
print("V (unbiased if polarization is low) = "+str(np.round(Vunbias*100,2))+"%")
print("Angle = "+str(np.round(polangle*180./PI,2))+" +/- "+str(np.round(polangle_err*180./PI,4))+" deg")
print("V S/N = "+str(np.round(V/V_err,0)))

# <codecell>

# non parametric KS test. Compare the difference in the distributions to make sure they are not different

ks_01 = stats.ks_2samp(im0, im1)
ks_35 = stats.ks_2samp(im3, im5)

"""Student's T-test on how the difference images
vary from a gaussian with mean 0. Can't use a two sample test because
each of the individual distributions are not necessarily gaussian, 
just the differences.

"""
diff_01 = im0-im1
diff_35 = im3-im5
t01 = stats.ttest_1samp(diff_01,0.0)
t35 = stats.ttest_1samp(diff_35,0.0)

"""
Mann Whitney U test. Essentially non-parametric T-test.
However only one sided and tests where X is less than Y. 
Since we only care about the difference we can take the abs value
and multiply that by two to get the two sided test.

Wilcoxon signed rank test is for paired data. Will work when comparing two pointings.

"""
mw01 = stats.mannwhitneyu(im0,im1)
mw35 = stats.mannwhitneyu(im3,im5)

mean_01 = np.mean(diff_01)
std_01 = np.std(diff_01)

mean_35 = np.mean(diff_35)
std_35 = np.std(diff_35)
print("Counts: 0="+str(np.sum(im0))+", 1="+str(np.sum(im1))+", 3="+str(np.sum(im3))+", 5="+str(np.sum(im5)))
print("KS Tests: 0-1: pval="+str(ks_01[1])+", 3-5: pval="+str(ks_35[1]))
print("1 Sample T Tests: 0-1: pval="+str(t01[1])+", 3-5: pval="+str(t35[1]))
print("Mann-Whitney U: 0-1: pval="+str(2*mw01[1])+", 3-5: pval="+str(2*mw35[1]))
#May want to start taking long tails into account
print("Mean 0-1 = "+str(mean_01)+" +/- "+str(std_01))
print("Mean 3-5 = "+str(mean_35)+" +/- "+str(std_35))

# <codecell>

"""Write to file"""
f = open(log_file,'a')
f.write("\n####New Entry\n")
f.write(str(fits_list)+'\n')
f.write("Source: "+source_region+'\n')
f.write("Background: "+bg_region+'\n')
f.write("Counts: 0="+str(np.sum(im0))+", 1="+str(np.sum(im1))+", 3="+str(np.sum(im3))+", 5="+str(np.sum(im5))+'\n')
f.write("Iavg = "+str(np.round(Iavg,0))+" I01 = "+str(np.round(I01/Iavg,3))+"Iavg I35 = "+str(np.round(I35/Iavg,3))+"Iavg\n")
f.write("Q = "+str(Q)+" +/- "+str(Q_err)+'\n')
f.write("U = "+str(U)+" +/- "+str(U_err)+'\n')
f.write("V = "+str(np.round(V*100.,2))+"% +/- "+str(np.round(V_err*100,4))+"%\n")
f.write("V (unbiased if polarization is low) = "+str(np.round(Vunbias*100,2))+"%\n")
f.write("Angle = "+str(np.round(polangle*180./PI,2))+" deg\n")
f.write("V S/N = "+str(np.round(V/V_err,0))+'\n')
f.write("KS Tests: 0-1: pval="+str(ks_01[1])+", 3-5: pval="+str(ks_35[1])+'\n')
f.write("1 Sample T Tests: 0-1: pval="+str(t01[1])+", 3-5: pval="+str(t35[1])+'\n')
f.write("Mann-Whitney U: 0-1: 2xpval="+str(2*mw01[1])+", 3-5: 2xpval="+str(2*mw35[1])+'\n')
f.write("Mean 0-1 = "+str(mean_01)+" +/- "+str(std_01)+'\n')
f.write("Mean 3-5 = "+str(mean_35)+" +/- "+str(std_35)+'\n')
f.close()

# <codecell>

binsize = 15
hist0 = plt.hist(im0,binsize,histtype='step',label="0deg")#, stacked=True)#
plt.hist(im1,binsize,histtype='step',label="90deg")#, stacked=True)
plt.hist(im3,binsize,histtype='step',label="45deg")
plt.hist(im5,binsize,histtype='step',label="135deg")
#plt.xlim(200,300)
plt.ylim(0,1.25*np.max(hist0[0]))
plt.xlabel("Counts [e-]")
plt.legend()
plt.savefig('E:/Documents/aastex52/polarization/figures/fourpolhist_M87V_jet.pdf',format='pdf')
plt.show()

# <codecell>

astrohist(diff_01, bins='knuth',histtype='step',label="0-1")
astrohist(diff_35, bins='knuth',histtype='step', label="3-5")
#plt.xlim(-200,200)
plt.xlabel("Counts [e-]")
plt.legend()
plt.savefig('E:/Documents/aastex52/polarization/figures/diffpolhist_M87V_jet.pdf',format='pdf')
plt.show()

# <codecell>

"""Compare 'same' images to make sure there are no issues with
weather"""

file_1 = 'E:/Science/OBS/2013-04-06/mM87_6600_0.241.fits'
image_1 = pyfits.open(file_1)
data1 = image_1[0].data # Make sure to grab the right FITS extension
data1 = data1.astype('float')

file_2 = 'E:/Science/OBS/2013-04-06/mM87_6600_0.250.fits'
image_2 = pyfits.open(file_2)
data2 = image_2[0].data # Make sure to grab the right FITS extension
data2 = data2.astype('float')

file_3 = 'E:/Science/OBS/2013-04-06/mM87_6600_0.267.fits'
image_3 = pyfits.open(file_3)
data3 = image_3[0].data # Make sure to grab the right FITS extension
data3 = data3.astype('float')


#x,y are flipped in pyfits arrays so NAXIS2,NAXIS1
shape = (image_1[0].header["NAXIS2"], image_1[0].header["NAXIS1"])

# because of dithering they won't be in the same position
region_string1 = """image
circle(1582.0,752.0,50.0)
"""
r1 = pyregion.parse(region_string1)
m1 = r1.get_mask(shape=shape)

region_string2 = """image
circle(1582.0,688.0,50.0)
"""
r2 = pyregion.parse(region_string2)
m2 = r2.get_mask(shape=shape)

region_string3 = """image
circle(1647.0,753.0,50.0)
"""
r3 = pyregion.parse(region_string3)
m3 = r3.get_mask(shape=shape)


data1 = data1*m1
data2 = data2*m2
data3 = data3*m3

#just used to verify we did the right masked region
filt_image = data2
hdu = pyfits.PrimaryHDU(filt_image)
hdulist = pyfits.HDUList([hdu])
hdulist.writeto('E:/masked_region.fits',clobber=True)


data1 = data1[data1 != 0]
data2 = data2[data2 != 0]
data3 = data3[data3 != 0]

dim = np.shape(data1)
print("Dimension is "+str(dim))
print("Number of pixels is "+str(np.sum(m1)))
print("Should be the same. Will give you an error running the K-S test otherwise.")
d1 = np.reshape(data1, dim)
d2 = np.reshape(data2, dim)
d3 = np.reshape(data3, dim)

# <codecell>

"""KS test regions for the 'same' image to make sure
they can all be used on the same test.
"""
ks12 = stats.ks_2samp(d2, d1)
ks13 = stats.ks_2samp(d3, d1)
ks23 = stats.ks_2samp(d2, d3)

print("1: "+str(np.round(np.mean(d1),2))+" +/- "+str(np.round(np.std(d1),2)))
print("2: "+str(np.round(np.mean(d2),2))+" +/- "+str(np.round(np.std(d2),2)))
print("3: "+str(np.round(np.mean(d3),2))+" +/- "+str(np.round(np.std(d3),2)))

print("KS 1 and 2: pval="+str(np.round(ks12[1],5)))
print("KS 1 and 3: pval="+str(np.round(ks13[1],5)))
print("KS 2 and 3: pval="+str(np.round(ks23[1],5)))
print(np.std(d1),np.std(d2),np.std(d3))

plt.hist(d1, 150,histtype='step',label="0-1")
plt.hist(d2, 150,histtype='step',label="0-1 shift 1")
plt.hist(d3, 150,histtype='step',label="0-1 shift 2")
plt.xlim(0,2000)
plt.xlabel("Counts [DN]")
plt.legend()
plt.savefig('E:/Documents/aastex52/polarization/figures/hist_sameposition.pdf',format='pdf')

# <codecell>

print(np.max(d1))
np.std(d2-d3),np.mean(d2-d3)

# <codecell>

np.sum(d1),np.sum(d2),np.sum(d3)

# <codecell>

#def mosaic_stich(**kwargs):
"""Combine images together

"""
"""Compare flats with polarizers to make sure they are stat id.
Can look at ones with another filter as well as just the polarizers
which were taken by themselves one night. Can get the throughput
from that as well.


"""
import numpy as np
import scipy.stats as stats
import pyfits

# To reset the vector uncomment next line
#result=None

# will append to the variable result to allow comparison between multiple nights.
for num in range(1,5):
	data1big=np.zeros((2048,1))
	data2big=np.zeros((2048,1))
	data3big=np.zeros((2048,1))
	data4big=np.zeros((2048,1))
	file_1 = 'E:/Science/OBS/2013-04-06/Flat1'+str(num)+'.fits'
	file_2 = 'E:/Science/OBS/2013-04-06/Flat2'+str(num)+'.fits'
	file_3 = 'E:/Science/OBS/2013-04-06/Flat3'+str(num)+'.fits'
	file_4 = 'E:/Science/OBS/2013-04-06/Flat4'+str(num)+'.fits'
	for count in range(1,5):
		image_1 = pyfits.open(file_1)
		data1 = image_1[count].data # Make sure to grab the right FITS extension
		data1 = data1.astype('float')
		data1big = np.hstack((data1big,data1))
		exptime1 = image_1[0].header["EXPTIME"]
		
		image_2 = pyfits.open(file_2)
		data2 = image_2[count].data # Make sure to grab the right FITS extension
		data2 = data2.astype('float')
		data2big = np.hstack((data2big,data2))
		exptime2 = image_2[0].header["EXPTIME"]
		
		image_3 = pyfits.open(file_3)
		data3 = image_3[count].data # Make sure to grab the right FITS extension
		data3 = data3.astype('float')
		data3big = np.hstack((data3big,data3))
		exptime3 = image_3[0].header["EXPTIME"]
		
		image_4 = pyfits.open(file_4)
		data4 = image_4[count].data # Make sure to grab the right FITS extension
		data4 = data4.astype('float')
		data4big = np.hstack((data4big,data4))
		exptime4 = image_4[0].header["EXPTIME"]
		
	data1 = data1big[:,1:]
	data2 = data2big[:,1:]
	data3 = data3big[:,1:]
	data4 = data4big[:,1:]
   
        data1[data1 == 'nan'] = np.nan
        data2[data2 == 'nan'] = np.nan
        data3[data3 == 'nan'] = np.nan
        data4[data4 == 'nan'] = np.nan
        
        dim = np.shape(data1)
        d1 = np.reshape(data1,dim[0]*dim[1])
        d2 = np.reshape(data2,dim[0]*dim[1])
        d3 = np.reshape(data3,dim[0]*dim[1])
        d4 = np.reshape(data4,dim[0]*dim[1])
        print(stats.nanmean(d1),stats.nanstd(d1))
        print(stats.nanmean(d2),stats.nanstd(d2))
        print(stats.nanmean(d3),stats.nanstd(d3))
        print(stats.nanmean(d4),stats.nanstd(d4))
        
        diff01 = (d1[~np.isnan(d1)]-d2[~np.isnan(d2)])
        diff35 = (d3[~np.isnan(d3)]-d4[~np.isnan(d4)])
        print(stats.ttest_1samp(diff01,0))
        print(stats.ttest_1samp(diff35,0))

        corr = 5.*2. # numimages = 5, e-/DN = 2
        
        d1 = d1[~np.isnan(d1)]*corr
        d2 = d2[~np.isnan(d2)]*corr
        d3 = d3[~np.isnan(d3)]*corr
        d4 = d4[~np.isnan(d4)]*corr

        print(stats.ttest_ind(d3,d4))

        val0 = np.mean(d1)
        val1 = np.mean(d2)
        val3 = np.mean(d3)
        val5 = np.mean(d4)
        
        im0_err = np.std(d1)
        im1_err = np.std(d2)
        im3_err = np.std(d3)
        im5_err = np.std(d4)

        I01 = (val0 + val1)/2.
        I35 = (val3 + val5)/2.
        Iavg = (I01 + I35)/2.
        Q = (val0-val1)/2.
        U = (val3-val5)/2.
        V = ((Q**2.+U**2.))**0.5/Iavg
        
        I01_err = 0.5*(im0_err**2.+im1_err**2.)**0.5
        I35_err = 0.5*(im3_err**2.+im5_err**2.)**0.5
        Iavg_err = 0.5*(I01_err**2.+I35_err**2.)**0.5

        Q_err = I01_err
        U_err = I35_err
        Vunbias = ((Q**2.+U**2.)**0.5 - (Q_err**2 + U_err**2)**0.5)/np.abs(Iavg)
        V_err = ((Q*Q_err)**2.+(U*U_err)**2.+(Iavg_err/Iavg)**2.)**0.5/(V*Iavg**2.)
        #print(np.mean(Q),np.mean(U),np.mean(V))
        
        try:
            result = np.vstack((result,[val0,V,V_err]))
        except NameError:
            #need to create an empty array to add into the first time
            result = np.zeros((1,3))
            result = np.vstack((result,[val0,V,V_err]))
            result = result[1:,:]
        

# <codecell>

plt.errorbar(result[:,0]/corr,result[:,1]*100.,yerr=result[:,2]*100)
plt.xlabel("Counts 0 Deg Image [DN]")
plt.ylabel("Percent Polarization")
plt.savefig('E:/Documents/aastex52/polarization/figures/domeflatpol.pdf',format='pdf')

# <codecell>

plt.hist(diff01, 700,histtype='step',label="0-1")
plt.hist(diff35, 250,histtype='step', label="3-5")
plt.xlim(-500,1500)
plt.xlabel("Counts")
plt.legend()
plt.savefig('E:/Documents/aastex52/polarization/figures/countdifference.pdf',format='pdf')

# <codecell>


