# Need to add input vectors
# Need to have options to start at different places based on input vectors
# Need to split the file creation (e.g. multi.lis)

def determine_spectral_annulus(binsize=None, annuli=None, centroid=None, infile=None,
								outfile=None, sourcename=None, bgname=None, obsid=None, ptsrc_file=None):
	"""Determine the size of the region needed to have a user inputed number of net counts.
	
	Keyword Arguments:
		binsize -- Net counts per bin. Typically 1000-2500 net count.
		annuli -- Total number of narrow annuli to create.
		centroid -- [x_center, y_center]
		infile -- Full path of the narrow radial bin file.
		outfile -- Output file prefix and output.
		sourcename -- Evt2 file associated with annuli.
		bgname -- Name of the reprojected and cleaned background file.
		obsid -- Obsid of the cluster.
		ptsrc_file -- Name of region file that contains the point sources.
	
	"""
	
	import csv 
	import os,math
	import numpy as np 
	import pylab as plt
	from ptsourcearea import point_source_area
	
	count_bin = 0
	annulus_inner = 0 # Final inner radius of the annulus for binsize counts.
	annulus_outer = 0 # Final outer radius of the annulus for binsize counts.

	if not annuli:
		annuli = int(raw_input('Enter number of NARROW annuli: '))
	if not centroid:
		x_center = float(raw_input('Enter X coordinate of Centroid: ')) 
		y_center = float(raw_input('Enter Y coordinate of Centroid: '))
	if centroid:
		x_center = centroid[0]
		y_center = centroid[1]
	if not binsize:
		binsize = int(raw_input('Enter the number of counts you want in each annulus: '))
	if not infile:
		infile = raw_input('Enter the full path for the narrow radial bins file in the form, innerradius, outer radius, net counts: ')
	if not outfile:
		outfile = raw_input('Enter the full output path: ')
		outprefix = raw_input('Enter the output prefix: ')
	if not sourcename:
		sourcename = raw_input('Enter name of evt2 file used in specextract: ')
	if not bgname:
		bgname = raw_input('Enter name of background file used in dmextract: ')
	if not obsid:	
		obsid = raw_input('Enter name of the OBSID: ') # This should already be included in the output prefix.
	if not ptsrc_file:	
		ptsrc_file = raw_input('Enter point source file: ')
	
	i = 0
	print("Number of annuli is "+str(annuli))
	print("Centroid is at ("+str(x_center)+","+str(y_center)+")")
	# Read line with annular inner, outer radius, and net counts.
	f = csv.reader(open(infile,'r'),delimiter = ',')
	x_bin_pos = []
	y_counts = []
	regions = []
	for count in range(annuli):
		line = f.next()	
		rad_inner = float(line[0])
		rad_outer = float(line[1])
		net_counts = float(line[2])
		count_bin = count_bin + net_counts
		x_bin_pos.append(count)
		y_counts.append(count_bin)
		if count_bin > binsize:
			annulus_outer = rad_outer
			# Close annulus at binsize counts. 
			count_bin = 0
			# Print region files for each of the annuli.
			with open(outfile+outprefix+'_'+str(obsid)+'_'+str(i)+'.reg','w') as reg:
				reg.write('annulus('+str(x_center)+','+str(y_center)+','+str(annulus_inner)+','+str(annulus_outer)+')')
			print("Annulus number "+str(i)+" from "+str(annulus_inner)+" to "+str(annulus_outer))		
			with open(outfile+'multi_src.lis','a') as src:
				src.write(sourcename+'[sky=region('+outprefix+str(i)+'_'+str(obsid)+'.reg)]'+'\n')
			with open(outfile+'multi_out.lis', 'a') as out:
				out.write(outprefix+str(obsid)+str(i)+'\n')
			with open(outfile+'multi_bkg.lis','a') as bkg:
				bkg.write(bgname+'[bin pi][sky=region('+outprefix+str(i)+'_'+str(obsid)+'.reg)]'+'\n')
			with open(outfile+'multi_bkg_out.lis','a') as bkgout:
				bkgout.write(outprefix+str(i)+'_'+str(obsid)+'_bkg.pi'+'\n')
			arcsec_inner = annulus_inner*0.492 # 0.492 "/pixel
			arcsec_outer = annulus_outer*0.492
			area = point_source_area(x_center,y_center,annulus_inner,annulus_outer,ptsrc_file) # subtract out point source area from point source file
			with open(outfile+'mass_profile_input_'+str(obsid)+'.txt','a') as mass:
				mass.write(outprefix+str(i)+'_'+str(obsid)+'.ai.fits '+str(arcsec_inner/60.)+' '+str(arcsec_outer/60.)+' '+str(area)+'\n')		
			regions.append([x_center,y_center,annulus_inner,annulus_outer,area])
			annulus_inner = annulus_outer # The current outer radius becomes the next inner radius.
			i += 1
	
	
	print("The number of leftover counts is "+str(count_bin))
	print("multi_src.lis and multi_out.lis for specextract source regions")
	print("multi_bkg.lis and multi_bkg_out.lis for dmextract background regions")
	print("mass_profile_input.txt for mass profile in arcmins")
	plt.plot(x_bin_pos,y_counts)
	plt.show()
	return regions