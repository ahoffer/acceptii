class Cluster():
		"""Define class for each galaxy cluster"""
		numberOfClusters = 0
		
		def __init__(self, inFile):
			print('this code is run when a cluster object is made')
			numberOfClusters += 1
			
			self.name = cluster_params['Name'] 
			self.obsid = cluster_params['Obsid'] 
			self.z = cluster_params['Redshift'] 
			self.nh = cluster_params['nH'] # 10^20 cm^-2
			
			
			#decide on method to name clusters
			macs0416 = Cluster()
			clusters = {'MACS0416' : myCluster1, 'name2' : myCluster2}
			theClusters = []
			theClusters.append(myCluster, myCluster2)
	
			myCluster1 = Cluster()
			myCluster2 = Cluster()
		
			theClusters[0].name
			theClusters[1].name
			clusters['name1'].name
			
		def __del__(self):
			numberOfClusters -= 1
			
		def cosmo_params(self): # use the redshift to derive cosmology scales
			""" Set the cosmological parameters."""
			self.cosmoparams = cosmocalc(self.redshift,H0=cosmology['H0'],WM=cosmology['WM'],WV=cosmology['omegaL'])
	
		def set_cluster_params(self,cluster_params=None):
			""" Set the parameters for the cluster
			
			Keyword Arguments:
			cluster_params -- dictionary which includes responses to ui questions 
			
			"""	
			if cluster_params:	
				self.name = cluster_params['Name'] 
				self.obsid = cluster_params['Obsid'] 
				self.z = cluster_params['Redshift'] 
				self.nh = cluster_params['nH'] # 10^20 cm^-2
			else:
				self.name = raw_input("Cluster Name: ")
				self.obsid = int(raw_input("Obsid: "))
				self.z = float(raw_input("Redshift: "))
				self.nh = float(raw_input("nH (10^20 cm^-2): "))
				
		def set_xspec_params(self, xspec_params=None):
			""" Set the Xspec parameters to be input.
			
			Keyword Arguments:
			cluster_params -- dictionary which includes responses to ui questions 
						
			"""
			if xspec_params:
				self.xspec_file = xspec_params['Xspecfile'] # Use full path
				self.reg_num = xspec_params['num_regions'] 
				self.reg_prefix = xspec_params['reg_prefix']  
				self.reg_suffix = xspec_params['reg_suffix']  		
			else:
				self.xspec_file = raw_input("Xspec full file path: ")
				self.reg_num = int(raw_input("Number of regions: "))
				self.reg_prefix = raw_input("Region prefix full path: ")
				self.reg_suffix = raw_input("Region suffix: ")
				
		def load_sql_data(self,cluster_name): # upload data to SQL database 
			""" SQL query to load the results from the database.""" 
			
		def get_sql_data(self,cluster_name): # get data from SQL database
			""" SQL query to get the results from the database.""" 
			
		def set_centroid(self,centroid=None,flag=0):
			"""Set the centroid of the cluster either by automatic routine or hand entered"""
	
			methods = {'hand': 0, 'Centroid': 1} # set different cases for different centroiding methods
	
			if centroid:
				x_cen = centroid[0]
				y_cen = centroid[1]
			if flag == 0:
				x_cen = float(raw_input('Enter X coordinate of Centroid: '))
				y_cen = float(raw_input('Enter Y coordinate of Centroid: '))
		
			return [xcen,ycen]
					