#need to add the x,y, and ellipse errors
#put x,y,ellipse on an xlog scale

def ellipse(ellipse_fits=None,xstart=0,ystart=0,bin=1,PIXSCAL=0.492):
	"""Plot ellipse data
		
	Keyword Arguments:
		ellise_fits -- FITS file name
		xstart -- Where you decide to start x center to plot relative difference
		ystart -- Where you decide to start y center to plot relative difference
		bin -- How much was the image binned to convert from image to physical
		pixscal -- scaling from physical to arcseconds 
	"""
	import pyfits
	import numpy as np

	# All data in image coordinates
	fluxtable = pyfits.open(ellipse_fits)
	ellipse_data = fluxtable[1].data

	x0 = np.array([]) # X center of ellipse 
	x0_err = np.array([])
	y0 = np.array([]) # Y center of ellipse 
	y0_err = np.array([])
	ellip = np.array([])
	ellip_err = np.array([])
	sma = np.array([])
	code = np.array([])
	pa = np.array([])
	count = 0
	 
	 
	 
	while count < len(fluxtable[1].data):
		x0 = np.append(x0,fluxtable[1].data[count][9]) 
		x0_err = np.append(x0_err,fluxtable[1].data[count][10]) 
		y0 = np.append(y0,fluxtable[1].data[count][11])
		y0_err = np.append(y0_err,fluxtable[1].data[count][12]) 
		ellip = np.append(ellip,fluxtable[1].data[count][5])
		ellip_err = np.append(ellip_err,fluxtable[1].data[count][6]) 
		sma = np.append(sma,fluxtable[1].data[count][0])
		code = np.append(code,fluxtable[1].data[count][37])
		pa = np.append(pa,fluxtable[1].data[count][7])
		count += 1
	
	# convert to arcseconds
	x0 = (x0 - xstart) * PIXSCAL * bin 
	x0_err = x0_err * PIXSCAL * bin
	y0 = (y0 - ystart) * PIXSCAL * bin
	y0_err = y0_err * PIXSCAL * bin
	sma = sma * PIXSCAL * bin
	
	# only plot ones that actually fit 
	sma = sma[code != 4]
	x0 = x0[code != 4]
	x0_err = x0_err[code != 4]
	y0 = y0[code != 4]
	y0_err = y0_err[code != 4]
	ellip = ellip[code != 4]
	ellip_err = ellip_err[code != 4]
	code = code[code != 4]
	pa = pa[code != 4]
	
	ellip_params = np.array([x0,x0_err,y0,y0_err,sma,ellip,ellip_err,code,pa])
	return(ellip_params)
	
def main():	
	import numpy as np
	import matplotlib.pyplot as plt
	params_soft = ellipse(ellipse_fits='E:/Science/Xray/11757/new/images/xray_05-2.fits',xstart=475.254,ystart=693.246,bin=2,PIXSCAL=0.492)
	params_broad = ellipse(ellipse_fits='E:/Science/Xray/11757/new/images/xray_05-7.fits',xstart=475.254,ystart=693.246,bin=2,PIXSCAL=0.492)
	params_hard = ellipse(ellipse_fits='E:/Science/Xray/11757/new/images/xray_2-7.fits',xstart=475.254,ystart=693.246,bin=2,PIXSCAL=0.492)
	
	#distance = (params_soft[0][:]**2 + params_soft[0][:]**2)**0.5
	plt.figure()

	plt.subplot(221)	
	plt.errorbar(params_soft[4][:],params_soft[0][:],yerr=params_soft[1][:],label='Soft')
	plt.errorbar(params_broad[4][:],params_broad[0][:],yerr=params_broad[1][:],fmt='g--',label='Broad')
	plt.errorbar(params_hard[4][:],params_hard[0][:],yerr=params_hard[1][:],fmt='r--',label='Hard')
	plt.xlabel("Semi-major Axis (arcsec)")
	plt.ylabel("Rel. X position (arcsec)")
	plt.xscale('log')
	plt.xlim(min(params_soft[4][:]),max(params_soft[4][:]))
	plt.ylim(min(params_soft[0][:])-1,max(params_soft[0][:])+1)
#	plt.legend(bbox_to_anchor=(0., 1.02, 1., .102), loc=3,ncol=2, mode="expand", borderaxespad=0.)
	
	plt.subplot(222)	
	plt.errorbar(params_soft[4][:],params_soft[2][:],yerr=params_soft[3][:])
	plt.errorbar(params_broad[4][:],params_broad[2][:],yerr=params_broad[3][:],fmt='g--')
	plt.errorbar(params_hard[4][:],params_hard[2][:],yerr=params_hard[3][:],fmt='r--')
	plt.xlabel("Semi-major Axis (arcsec)")
	plt.ylabel("Rel. Y position (arcsec)")
	plt.xscale('log')
	plt.xlim(min(params_soft[4][:]),max(params_soft[4][:]))
	plt.ylim(-6,max(params_soft[2][:])+1)
	
	plt.subplot(223)	
	plt.plot(params_soft[0][:],params_soft[2][:])
	plt.plot(params_broad[0][:],params_broad[2][:],'g--')
	plt.plot(params_hard[0][:],params_hard[2][:],'r--')
	#plot mean (x,y) positions
	plt.plot(np.mean(params_soft[0][:]),np.mean(params_soft[2][:]),'o',markersize=5,color='b')
	plt.plot(np.mean(params_broad[0][:]),np.mean(params_broad[2][:]),'o',markersize=5,color='g')
	plt.plot(np.mean(params_hard[0][:]),np.mean(params_hard[2][:]),'o',markersize=5,color='r')
	plt.plot(0,0,'+',markersize=15,color='r')#plot original center
	plt.xlabel("Rel. X position (arcsec)")
	plt.ylabel("Rel. Y position (arcsec)")
	plt.xlim(min(params_soft[0][:])-1,max(params_soft[0][:])+1)
	plt.ylim(-6,max(params_soft[2][:])+1)
	
	plt.subplot(224)	
	plt.errorbar(params_soft[4][:],params_soft[5][:],yerr=params_soft[6][:])
	plt.errorbar(params_broad[4][:],params_broad[5][:],yerr=params_broad[6][:],fmt='g--')
	plt.errorbar(params_hard[4][:],params_hard[5][:],yerr=params_hard[6][:],fmt='r--')
	plt.xlabel("Semi-major Axis (arcsec)")
	plt.ylabel("Ellipticity")
	plt.xscale('log')
	plt.xlim(min(params_soft[4][:]),max(params_soft[4][:]))
	plt.ylim(0,1)
	
	plt.savefig('E:/Documents/aastex52/rxj2014/centroid_shifting.pdf',format='pdf')

if __name__ == "__main__":
    main()