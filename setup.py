from distutils.core import setup
setup(name='acceptii',
      version='0.1',
      description='ACCEPT II Cluster Analysis Pipeline',
      author='Aaron Hoffer',
      url='https://bitbucket.org/ahoffer/acceptii/src/',
      author_email='hofferaa@msu.edu',
      py_modules=['acceptii', 'deproj'],
      )