def two_d():
	""""Run the 2D modules through here"""
	
	
	def cont_bin(SN=50):
		"""Run cont_bin by Sanders 
		http://www-xray.ast.cam.ac.uk/papers/contbin/
		
		Keyword Arguments:
			SN -- Set the S/N threshold for creating a region
			--out=FILE 

		  Specify an output filename for the binned image. This is the input
		  image binned with the generated bins. Default is contbin_out.fits

			--outsn=FILE

		  Specify output image which shows the final signal to noise for each
		  bin, in each pixel of each bin.

		--outbinmap=FILE

		  This is a generated binmap. The binmap is a fits image, where each
		  pixel in the input image has been replaced by a number specifying
		  which bin that pixel is in. The bins are numbered from zero. Region
		  files can be produced from this file by make_region_files.

		--bg=FILE

		  This is a counts image with a background image to use for the signal
		  to noise calculations. This can have a different exposure to the
		  input image (make sure the EXPOSURE keyword is set correctly). If
		  the input and background images have exposures varying as a function
		  of position (e.g. they are the result of several images added
		  together), the --expmap and --bgexpmap options can specify FITS
		  images where the exposure of each pixel is given (in the same
		  units).

		--mask=FILE

		  An image to exclude certain regions of the image from binning
		  (e.g. point sources, regions beyond the edge of the CCD). Pixels in
		  the image should be 1 to be included, and 0 elsewhere.

		  A good way of making the image is to take the input image and set
		  each of the pixels to 1. farith (FTOOLS) could be used here.

		  farith inimage.fits 0 temp.fits MUL
		  farith temp.fits 1 allones.fits ADD
		  rm temp.fits

		  You would make a region file with ds9 to specify the included
		  regions and make the mask using (CIAO):

		  dmcopy "allones.fits[sky=region(myreg.reg)][opt full]" mask.fits

		  Alternatively, ask me for a program to make them directly from
		  region files.

		--smoothed=FILE

		  Rather than accumulatively smoothing the input image with the
		  background image in the program, an smoothed image can be specified
		  here. For instance, this could be an csmooth output image, or a
		  smoothed hardness map. accumulate_smooth_expmap or accumulate_smooth
		  could be used to produce this (if you do a lot of binning, it helps
		  to split the smoothing and binning operations timewise).

		--expmap=FILE
		--bgexpmap=FILE

		  Specify exposure map foreground and background images (see the --bg
		  option above for details)

		--sn=VAL

		  Specify the minimum signal to noise of each bin. This is t_b in the
		  paper. If there is no background, this is approximately the square
		  root of the number of counts.

		--automask

		  Automatically try to remove unused regions of the input image, so
		  that a mask image does not need to be supplied. This works by
		  removing 8x8 pixel regions from the input image which do not contain
		  any counts. This option is designed for a "quick look" at the
		  binning process, if you haven't made a mask image.

		--constrainfill

		  Enable the geometric constraint in the binning process, as described
		  in the paper. This ensures that the bins do not become too
		  elongated. The constraint parameter should be set with
		  --constrainval=X

		--constrainval=VAL

		  Set the geometric constraint value. --constrainfill has to be
		  specified for this to have any effect. If a bin currently has N
		  pixels, the program calculates the radius of a circle (r) with that
		  area. It will not add any new pixels greated than VAL*r away from
		  the current flux-weighted centroid of the bin. If VAL is around 1
		  then the bins are approximately spherical. Typical values are 2-3.

		--smoothsn=VAL

		  Signal to noise to smooth the image by before binning. This is 15 by
		  default. Larger values make smoothed-edged bins, but may miss small
		  features.

		--noscrub

		  An option to leave out the scrubbing process which removes small
		  bins below the signal to noise threshold. This is for testing
		  purposes.

		--binup

		  The program bins the image binning using the highest pixel in the
		  smoothed map first. This reverses this, binning from the lowest
		  pixel first. This is useful if binning using a colour map.

		--help

		  Shows the various options

		--version

		  Which version of the program this is

				"""
		
		return contours
	
	