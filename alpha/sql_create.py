"""Run this file once when you create the databases"""
import sqlite3

# clusters.db includes all the ACCEPT2 related tables
conn = sqlite3.connect('E:/Scripts/ACCEPT2/repros/acceptii/cluster_attrib.db')
c = conn.cursor()

# Physical cluster attributes and identification numbers
c.execute('''CREATE TABLE clusters (name TEXT, ra TEXT, dec TEXT, redshift REAL, nh REAL, obsid INTEGER)''')

# surbri table includes surface brightness profiles radii in arcseconds fluxes are bg subtracted in counts/second
c.execute('''CREATE TABLE surbri (name TEXT, rad_id INTEGER, inner REAL, outer REAL, sb REAL, energy_low REAL, energy_high REAL, FOREIGN KEY(name) REFERENCES clusters(name))''')

#xspec table includes the results of the Xspec fits
c.execute('''CREATE TABLE xspec (name TEXT, rad_id INTEGER, inner REAL, outer REAL, Tx REAL, Tx_low REAL, Tx_high REAL, Fe REAL, Fe_low REAL, Fe_high REAL, norm1 REAL, lumin REAL, lumin_low REAL, lumin_high REAL, cr REAL, dof REAL, stat REAL, stattype TEXT, FOREIGN KEY(name) REFERENCES clusters(name))''')

#accept table includes the results of the accept style fits interpolated
c.execute('''CREATE TABLE accept (name TEXT, rad_id INTEGER, inner REAL, outer REAL, nelec REAL, nelec_low REAL, nelec_high REAL, entropy REAL, entropy_low REAL, entropy_high REAL, k0 REAL, k100 REAL, alpha REAL, FOREIGN KEY(name) REFERENCES clusters(name))''')

conn.close()