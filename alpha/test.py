#Test modules here before adding them to other pieces
def download_obsid(obsid):
	""""Download the obsid from Chaser
	http://cxc.harvard.edu/ciao/threads/archivedownload/
	"""
	import subprocess
	DOWNLOAD = "download_chandra_obsid "+str(obsid)
	p = subprocess.Popen(DOWNLOAD,shell=True,stdout=subprocess.PIPE,stderr=subprocess.PIPE)
	(sout,serr) = p.communicate()
	print(sout,serr)
	if serr:
		error = "There was an error getting your data " + serr
		return error
	else:
		return "####OBSID "+str(obsid)+" has been downloaded\n\n"