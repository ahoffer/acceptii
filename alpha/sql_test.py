# Need to update for bulk uploading
# Need to update for updating entries
import sqlite3

#cluster_attrib.db created by sql_create.py with 3 tables 
conn = sqlite3.connect('E:/Scripts/ACCEPT2/repros/acceptii/cluster_attrib.db')
c = conn.cursor()

def insert_clusters(cluster=None):
	"""Insert into cluster table. There are 6 elements."""
	if cluster:	
		c.execute('INSERT INTO clusters VALUES (?,?,?,?,?,?)',cluster)
		conn.commit()
		return "Values inserted into surbri table"
	else:
		return "No values entered"

def get_clusters(cluster_name=None):
	"""Get all values from clusters by calling the cluster name."""
	if cluster_name:
		c.execute('SELECT * FROM clusters WHERE name=?', [cluster_name])
		return c.fetchall()

def insert_surbri(sur_bri=None):
	"""Insert into surface brightness table there are 7 elements"""	
	if sur_bri:
		c.execute('INSERT INTO surbri VALUES (?,?,?,?,?,?,?)',sur_bri)
		conn.commit()
		return "Values inserted into surbri table"
	else:
		return "No values entered"

def get_surbri(cluster_name=None):
	"""Get values from surbri table"""
	if cluster_name:
		c.execute('SELECT * FROM surbri WHERE name=?', [cluster_name])
		return c.fetchall()

def insert_xspec(xspec=None):
	"""Insert into xspec table. There are 18 elements."""
	if xspec:
		c.execute('INSERT INTO xspec VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)',xspec)
		conn.commit()
		return "Values inserted into xspec table"
	else:
		return "No values entered"

def get_xspec(cluster_name=None):
	"""Get values from xspec table"""
	if cluster_name:
		c.execute('SELECT * FROM xspec WHERE name=?', [cluster_name])
		return c.fetchall()

def insert_accept(accept=None):
	"""Insert into accept table there are 13 elements."""
	if accept:
		c.execute('INSERT INTO accept VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)',accept)
		conn.commit()
		return "Values inserted into accept table"
	else:
		return "No values entered"

def get_accept(cluster_name=None):
	"""Get values from accept table"""
	if cluster_name:
		c.execute('SELECT * FROM accept WHERE name=?', [cluster_name])
		return c.fetchall()

def close():
	conn.close()


#def jsonify_cluster(cluster_dict=None):
#	"""Convert the output into JSON string"""
#	import json
#	if cluster_dict:
#		return json.dumps(cluster_dict, sort_keys=True) 		
