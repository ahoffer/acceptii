def anngen(x_cen=None,y_cen=None,ann_max=None,ann_size=None, out_dir=None):
	"""Script to create evenly spaced surface brightness lists.
 
	Keyword Arguments: 
		x_cen -- X position of centroid for cluster 
		y_cen -- Y position of centroid for cluster
		ann_max -- define how far out you want to go
		ann_size -- thickness of annulus
		out_dir -- output directory
    """

	# possibly make mode to edge of chip later?

	if not x_cen:
		x_cen = float(raw_input("Enter the x centroid: "))
	if not y_cen:
		y_cen = float(raw_input("Enter the y centroid: "))
	if not max_rad:
		max_rad = float(raw_input("Enter the max_radius: "))
	if not obsid:
		obsid = int(raw_input("Enter OBSID: "))
	
	rad = 0.0
	count = 0
	out_file = out_dir+str(obsid)+'_surbri.reg'
	f_annulus_list = open(out_file,'w')

	while(rad < max_rad):
		rad_in = rad
		rad += ann_size
		line = "annulus("+str(x_cen)+","+str(y_cen)+","+str(rad_in)+","+str(rad)+")\n"
		f_annulus_list.write(line)
		count += 1
	
	f_annulus_list.close()
	
	staus = "Narrow annulus file " + str(out_file) + " created."
	return status  # Should I just return annulus?
