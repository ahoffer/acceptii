"""Load in hardband and softband files and compute the hardness ratio and send to an image"""
import pyfits
import numpy as np
hard_file = 'E:/Documents/aastex52/rxj2014/hard.fits'
soft_file = 'E:/Documents/aastex52/rxj2014/soft.fits'
hard_image = pyfits.open(hard_file)
soft_image = pyfits.open(soft_file)
hard_data = hard_image[0].data
hard_header = hard_image[0].header
soft_data = soft_image[0].data
hard_data = hard_data.astype('float')
soft_data = soft_data.astype('float')

hardness = np.divide((hard_data - soft_data),(hard_data + soft_data))
hardness_err = 2*(hard_data + soft_data)**(-2.)*(hard_data*soft_data**2 + soft_data*hard_data**2)**0.5

SN = np.divide(hardness,hardness_err)

hardness[np.absolute(SN) < 5] = np.nan
pyfits.writeto('E:/Documents/aastex52/rxj2014/hardness.fits',hardness,header=hard_header, clobber=True)
pyfits.writeto('E:/Documents/aastex52/rxj2014/hardness_SN.fits',SN,header=hard_header, clobber=True)